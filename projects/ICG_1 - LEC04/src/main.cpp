// .zip folder provided by the professor
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <fstream> //03
#include <string> //03

#include <GLM/glm.hpp> //04
#include <GLM/gtc/matrix_transform.hpp> // used for transformations.


GLFWwindow* window; // global because we have this in many places.

bool initGLFW() {
	if (glfwInit() == GLFW_FALSE) {
		std::cout << "Failed to Initialize GLFW" << std::endl;
		return false;
	}

	//Create a new GLFW window
	window = glfwCreateWindow(500, 500, "ICG_1 - LEC04", nullptr, nullptr);
	glfwMakeContextCurrent(window);

	return true;
}

bool initGLAD() {
	if (gladLoadGLLoader((GLADloadproc)glfwGetProcAddress) == 0) {
		std::cout << "Failed to initialize Glad" << std::endl;
		return false;
	}
}


GLuint shader_program;

// many ways of loading a shader from a file. This is what the professor decided on.
bool loadShaders() {
	// Read Shaders from file
	std::string vert_shader_str;
	std::ifstream vs_stream("vertex_shader.glsl", std::ios::in);
	if (vs_stream.is_open()) {
		std::string Line = "";
		while (getline(vs_stream, Line))
			vert_shader_str += "\n" + Line;
		vs_stream.close();
	}
	else {
		printf("Could not open vertex shader!!\n");
		return false;
	}
	const char* vs_str = vert_shader_str.c_str();

	std::string frag_shader_str;
	std::ifstream fs_stream("frag_shader.glsl", std::ios::in);
	if (fs_stream.is_open()) {
		std::string Line = "";
		while (getline(fs_stream, Line))
			frag_shader_str += "\n" + Line;
		fs_stream.close();
	}
	else {
		printf("Could not open fragment shader!!\n");
		return false;
	}
	const char* fs_str = frag_shader_str.c_str();

	GLuint vs = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vs, 1, &vs_str, NULL);
	glCompileShader(vs);
	GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fs, 1, &fs_str, NULL);
	glCompileShader(fs);

	shader_program = glCreateProgram();
	glAttachShader(shader_program, fs);
	glAttachShader(shader_program, vs);
	glLinkProgram(shader_program);

	return true;
}


int main() {
	//Initialize GLFW
	if (!initGLFW())
		return 1;
	//Initialize GLAD
	if (!initGLAD())
		return 1;

	// Built directly into this since this is just a tester program.
	// Triangle data
	static const GLfloat points[] = {
		-1.0f, -1.0f, 0.0f,
		1.0f, -1.0f, 0.0f,
		0.0f, 1.0f, 0.0f
	};

	// Color data
	static const GLfloat colors[] = {
		1.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 1.0f
	};

	// VBO - eperate for position and colour.
	GLuint pos_vbo = 0;
	glGenBuffers(1, &pos_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, pos_vbo);
	glBufferData(GL_ARRAY_BUFFER, 9 * sizeof(float), points, GL_STATIC_DRAW);

	GLuint color_vbo = 0;
	glGenBuffers(1, &color_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, color_vbo);
	glBufferData(GL_ARRAY_BUFFER, 9 * sizeof(float), colors, GL_STATIC_DRAW);

	// VAO
	GLuint vao = 0;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, pos_vbo);

	//			(index, size, type, normalized, stride, pointer)
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	//			(stride: byte offset between consecutive values)
	//			(pointer: offset of the first component of the 
	//			first attribute in the array - initial value is 0)

	glBindBuffer(GL_ARRAY_BUFFER, color_vbo);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

	// Load your shaders
	if (!loadShaders())
		return 1;


	////////////////////////
	int width, height;

	// (window handle, window_width, window_height
	glfwGetWindowSize(window, &width, &height);

	// projection matrix (PROJECTION MATRIX)
	// creating a projection matrix that will multiplied by the model and view matrices
	// glm::perspective (angle_radians, aspect_ratio, near_clipping_plane
	// - anything between the camera (0) and the near clipping plane will not show up
	// - far clipping plane - anything past this will not be seen.
	glm::mat4 Projection = glm::perspective(glm::radians(45.0f), (float)width / (float)height, 0.1f, 100.0f);

	// camera matrix (VIEW MATRIX)
	// glm::lookAt(glm::vec3 camera_position, glm::vec3 camera_target, glm::vec3 orientation
	// - the camera is a little bit to the right, a little bit up, a little far back from the object.
	// - the camera's target; currently pointing to the origin.
	// - camera origin; currently pointing up in the y-direction
	// glm::mat4 View = glm::lookAt(glm::vec3(2, 2, 3), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0)); // original
	glm::mat4 View = glm::lookAt(glm::vec3(0, 0, 3), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0)); // regular

	// model matrix (MODEL MATRIX)
	// Transformations (Scaling, Rotating, and Translating)
	// By providing a value of (1.0F), we are creating an identity matrix.
	// Whenever an identity matrix is applied, the end result is the same.
	glm::mat4 Model = glm::mat4(1.0f);

	Model = glm::translate(Model, glm::vec3(1.5f, 0.0f, 0.0f)); // translates the object

	// MVP
	// matrix we're sending into the shader
	glm::mat4 mvp = Projection * View * Model;

	// linking our shader (must be referenced as MVP_
	GLuint matrixID = glGetUniformLocation(shader_program, "MVP");

	////////////////////////


	///// Game loop /////
	while (!glfwWindowShouldClose(window)) {
		glfwPollEvents();

		glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		glUseProgram(shader_program);

		// set as uniform since the object doesn't change.
		glUniformMatrix4fv(matrixID, 1, GL_FALSE, &mvp[0][0]);
		//glBindVertexArray(vao);
		// draw points 0-3 from the currently bound VAO with current in-use shader
		glDrawArrays(GL_TRIANGLES, 0, 3);


		glfwSwapBuffers(window);
	}
	return 0;

}