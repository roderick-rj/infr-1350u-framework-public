#version 410
layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inColor;
layout(location = 2) in vec3 inNormal;

layout(location = 0) out vec3 outWorldPos;
layout(location = 0) out vec3 outColor;
layout(location = 0) out vec3 outNormal;

out vec3 CamDirection;
out vec3 LightDirection;

uniform mat4 MVP;
uniform mat4 View;
uniform mat4 Model;
uniform vec3 LightWorldPos;

void main() {
	gl_Position = MVP * vec4(inPosition, 1.0);

	// Pass vertex pos to frag shader
	outWorldPos = (Model * vec4(inPosition, 1)).xyz;

	// We need camera direction (could be one line; spaced out for readability)
	vec3 vertexPos = (View * Model * vec4(inPosition, 1)).xyz; 
	CamDirection = vec3(0.0, 0.0, 0.0) - vertexPos;

	// Light direction (all in camera space)
	vec3 lightPos = (View * Model * vec4(LightWorldPos, 1)).xyz;
	LightDirection = lightPos + CamDirection;

	// Normals
	outNormal = (View * Model * vec4(inNormal, 0)).xyz; //this was (inNormal, 1) - wrong!!
	outColor = inColor; // I forgot to output the color :( just add this line
}
	