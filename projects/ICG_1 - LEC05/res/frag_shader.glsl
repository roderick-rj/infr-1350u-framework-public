#version 410

layout(location = 0) in vec3 inWorldPos;
layout(location = 1) in vec3 inColor;
layout(location = 2) in vec3 inNormal;

in vec3 CamDirection;
in vec3 LightDirection;

uniform vec3 LightWorldPos;

out vec4 frag_color;

void main() {
	vec3 lightColor = vec3(1.0, 1.0, 1.0);
	float lightIntensity = 5.0f;

	vec3 diffuseColor = inColor;
	vec3 ambientColor = vec3(0.0, 0.0, 0.0);
	vec3 specularColor = vec3(0.5, 0.5, 0.5);

	// Normal
	vec3 n = normalize(inNormal);

	vec3 l = normalize(LightDirection);

	vec3 E = normalize(CamDirection);

	// Halfway vector
	vec3 H = normalize();

	// Specular intensity
	float spec = pow(max(dot(n, H), 0.0), 256);

	// Specular component
	vec3 specOut = spec * specularColor;

	// Distance between the light and position in world space.
	// Used so we can attenuate it and control the distance of the light.
	float dist = length(LightWorldPos - inWorldPos);

	// Diffuse Component
	float diffuseComponent = max(dot(n, l), 0.0);
	// Attenuates it so that the light strength varies based on distance fom it.
	vec3 diffuseOut = (diffuseComponent * diffuseColor) / (dist * dist);

	// Ambient Component
	vec3 ambientOut = ambientColor * 0.0; // shininess factor

	// this is where we'd get our texture. We do xyz since we aren't getting the alpha here.
	vec3 result = (ambientOut + diffuseOut + specOut) 
		* lightIntensity * inColor.xyz;

	frag_color = vec4(result, 1.0);
}