#pragma once
#include "Primitive.h"

typedef class PrimitiveCube : public Primitive
{
public:
	// sets a single side length for width, height, and depth
	PrimitiveCube(float sideLength = 1.0F);

	// sets the width, height, and depth
	PrimitiveCube(float width = 1.0F, float height = 1.0F, float depth = 1.0F);

private:
	float width; // width of box
	float height; // height of box
	float depth; // thickness of box
protected:
} Cube;
