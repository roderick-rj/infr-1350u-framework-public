#pragma once
#include "Primitive.h"


typedef class PrimitivePlane : public Primitive
{
public:
	PrimitivePlane(float width = 1.0F, float height = 1.0F);

	// PrimitivePlane(float width, float height);

private:
	float width;
	float height;

protected:

} Plane;

