/*
 * Name: Roderick "R.J." Montague
 * Student Number: 100701758
 * Date: 11/04/2019
 * Description: the main class that connects all of the graphics functions together to make the game work.
*/

// GAME (SOURCE)
#include "Game.h"
#include <stdexcept>
#define IMGUI_IMPL_OPENGL_LOADER_GLAD
#include <imgui.h>
#include <imgui_impl_opengl3.h>
#include <imgui_impl_glfw.h>
#include <GLM/gtc/matrix_transform.hpp>

#include <functional>

#include "SceneManager.h"
#include "MeshRenderer.h"

#include "utils/math/Collision.h" // collision functionality
#include "utils/math/Rotation.h" // rotation functionality
// #include <string>

struct TempTransform {
	glm::vec3 Position = glm::vec3(0.0f);
	glm::vec3 EulerRotation = glm::vec3(0.0f);
	glm::vec3 Scale = glm::vec3(1.0f);

	// does our TRS for us.
	glm::mat4 GetWorldTransform() const {
		return
			glm::translate(glm::mat4(1.0f), Position) *
			glm::mat4_cast(glm::quat(glm::radians(EulerRotation))) *
			glm::scale(glm::mat4(1.0f), Scale)
			;
	}
};

struct UpdateBehaviour {
	std::function<void(entt::entity e, float dt)> Function;
};

// call this function to  resize the window.
void GlfwWindowResizedCallback(GLFWwindow* window, int width, int height) {
	glViewport(0, 0, width, height);
	Game* game = (Game*)glfwGetWindowUserPointer(window);

	if (game != nullptr)
	{
		game->Resize(width, height);
	}
}

// called when a mouse button event is recorded
void MouseButtomCallback(GLFWwindow* window, int button, int action, int mods) {
	Game* game = (Game*)glfwGetWindowUserPointer(window);

	// returns the function early if this isn't a game class
	if (game == nullptr) {
		return;
	}

	// calls designated mouse button functions
	switch (action) {
	case GLFW_PRESS:
		game->MouseButtonPressed(window, button);
		break;
	case GLFW_RELEASE:
		game->MouseButtonReleased(window, button);
		break;
	}
}

// called when a key has been pressed, is being held down, or when a key has been released 
// This function figures out which one it is, and calls the appropriate function to handle it.
void KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	Game* game = (Game*)glfwGetWindowUserPointer(window);

	if (game != nullptr) // checks to see if the Game object exists
	{
		// checks for what type of button press happened.
		switch (action)
		{
		case GLFW_PRESS: // key has been pressed
			game->KeyPressed(window, key);
			break;

		case GLFW_REPEAT: // key is held down
			game->KeyHeld(window, key);
			break;

		case GLFW_RELEASE: // key has been released
			game->KeyReleased(window, key);
			break;
		}
	}
}



Game::Game() :
	myWindow(nullptr),
	myWindowTitle("ICG_1 - Exam - 100701758"),
	myClearColor(glm::vec4(0.1f, 0.7f, 0.5f, 1.0f))
{ }

Game::~Game() { }

void Game::Initialize() {
	// Initialize GLFW
	if (glfwInit() == GLFW_FALSE) {
		std::cout << "Failed to initialize GLFW" << std::endl;
		throw std::runtime_error("Failed to initialize GLFW");
	}
	// Enable transparent backbuffers for our windows (note that Windows expects our colors to be pre-multiplied with alpha)
	glfwWindowHint(GLFW_TRANSPARENT_FRAMEBUFFER, true);
	// Create a new GLFW window
	myWindow = glfwCreateWindow(700, 700, myWindowTitle, nullptr, nullptr);
	// We want GL commands to be executed for our window, so we make our window's context the current one
	glfwMakeContextCurrent(myWindow);
	// Let glad know what function loader we are using (will call gl commands via glfw)
	if (gladLoadGLLoader((GLADloadproc)glfwGetProcAddress) == 0) {
		std::cout << "Failed to initialize Glad" << std::endl;
		throw std::runtime_error("Failed to initialize GLAD");
	}

	// setting up window user pointer so that we can resize our window
	// Tie our game to our window, so we can access it via callbacks
	glfwSetWindowUserPointer(myWindow, this);
	glEnable(GL_DEPTH_TEST);
	// Set our window resized callback
	glfwSetWindowSizeCallback(myWindow, GlfwWindowResizedCallback);
	
	// Setting keyboard callback function
	glfwSetKeyCallback(myWindow, KeyCallback);

	// Setting mouse button callback function
	glfwSetMouseButtonCallback(myWindow, MouseButtomCallback);

}

void Game::Shutdown() {
	glfwTerminate();
}

// loads the content for the meshes and shaders
void Game::LoadContent() 
{
	const unsigned int cubeVertsCount = 8; // total amount of vertices for a cube
	const unsigned int cubeIndsCount = 36; // total amount of indices for a cube

	// creates the cube indicies.
	// the labels for each row may possibly be incorrect, but the cube gets made properly anyway
	uint32_t cubeInds[cubeIndsCount] = {
		0, 1, 2, // front face - top half
		2, 1, 3, // front face - bottom half
		0, 4, 2, // right side-face (box's perspective) - top half
		2, 4, 6, // right side-face (box's perspectice) - bottom half
		1, 5, 3, // left side-face - top half
		3, 5, 7, // left side-face, bottom half
		4, 5, 6, // back face - top half
		6, 5, 7, // back face - bottom half
		0, 4, 5, // top tri 1
		5, 0, 1, //  top tri 2
		2, 6, 7, // bottom tri 1
		7, 2, 3 // bottom tri 2
	};

	myCamera = std::make_shared<Camera>();
	myCamera->SetPosition(glm::vec3(5, 3, 30));
	myCamera->LookAt(glm::vec3(0.0f, 0.0f, 0.0f));

	// sets the camera to perspective mode.
	cameraAngle = glm::radians(60.0f);
	myCamera->Projection = glm::perspective(cameraAngle, 1.0f, 0.01f, 1000.0f);
	perspectiveCamera = true; // in perspective mode.

	// MESH 1 (RAINBOW PLANE)
	// plane vertices
	Vertex bgVerts[4] = {
		// Position				  Color							Normal
		//  x      y	 z		   r	 g	   b	 a		   x  y  z
		{{ -35.0f, -35.0f, -35.0f }, { 1.0f, 1.0f, 1.0f, 1.0f }, {0, 0, 1}},
		{{ 35.0f, -35.0f, -35.0f },  { 0.0f, 0.0f, 1.0f, 1.0f }, {0, 0, 1}},
		{{ -35.0f, 35.0f, -35.0f },  { 0.0f, 1.0f, 0.0f, 1.0f }, {0, 0, 1}},
		{{ 35.0f, 35.0f, -35.0f },   { 1.0f, 0.0f, 0.0f, 1.0f }, {0, 0, 1}},
	};

	// plane indices
	uint32_t bgInds[6] = {
	0, 1, 2,
	2, 1, 3
	};

	
	// Create a new mesh from the plane data
	myMesh = std::make_shared<Mesh>(bgVerts, 4, bgInds, 6);
	
	// MESH 2 and MESH 3 (PLAYERS)
	const unsigned int plyrVertsTotal = 8; // total vertices
	const unsigned int plyrIndsTotal = 36; // total indices

	// the vertices for a player object. Player 1 and player 2 are both the same size. 
	// The two players are moved to their proper places after their vertices are set.
	Vertex plyrVerts[plyrVertsTotal] = {
		// Position				  Color							Normal
		//  x      y	 z		   r	 g	   b	 a		   x  y  z
		{{ -pWidth / 2.0F, -pHeight / 2.0F,  pDepth / 2.0F}, { 1.0f, 1.0f, 1.0f, 1.0f }}, // bottom left, front corner
		{{ -pWidth / 2.0F,  pHeight / 2.0F,  pDepth / 2.0F}, { 1.0f, 1.0f, 1.0f, 1.0f }}, // top left, front corner
		{{ -pWidth / 2.0F, -pHeight / 2.0F, -pDepth / 2.0F}, { 1.0f, 1.0f, 1.0f, 1.0f }}, // bottom left, back corner
		{{ -pWidth / 2.0F,  pHeight / 2.0F, -pDepth / 2.0F}, { 1.0f, 1.0f, 1.0f, 1.0f }}, // top left, back corner

		{{ pWidth / 2.0F, -pHeight / 2.0F,  pDepth / 2.0F}, { 1.0f, 1.0f, 1.0f, 1.0f }}, // bottom right, front corner
		{{ pWidth / 2.0F,  pHeight / 2.0F,  pDepth / 2.0F}, { 1.0f, 1.0f, 1.0f, 1.0f }}, // top right, front corner
		{{ pWidth / 2.0F, -pHeight / 2.0F, -pDepth / 2.0F}, { 1.0f, 1.0f, 1.0f, 1.0f }}, // bottom right, back corner
		{{ pWidth / 2.0F,  pHeight / 2.0F, -pDepth / 2.0F}, { 1.0f, 1.0f, 1.0f, 1.0f }} // top right, back corner
	};
	
	// the vertices for player 1 and player 2
	Vertex p1Verts[plyrVertsTotal]; // vertices for p1
	Vertex p2Verts[plyrVertsTotal]; // vertices for p2


	// indices for a player object.
	// I think I was planning on doing something different earlier on, hence why the players have their own indice array.
	// the values are the same as they are in the 'cubeInds' array, so there's no real reason for it to exist.
	uint32_t plyrInds[plyrIndsTotal] = {
		0, 1, 2, // front face - top half
		2, 1, 3, // front face - bottom half
		0, 4, 2, // right side-face (box's perspective) - top half
		2, 4, 6, // right side-face (box's perspectice) - bottom half
		1, 5, 3, // left side-face - top half
		3, 5, 7, // left side-face, bottom half
		4, 5, 6, // back face - top half
		6, 5, 7, // back face - bottom half
		0, 4, 5, // top tri 1
		5, 0, 1, //  top tri 2
		2, 6, 7, // bottom tri 1
		7, 2, 3 // bottom tri 2
	};

	// sets the vertex positions for both players. The default player positions are set later.
	for (int i = 0; i < plyrVertsTotal; i++)
	{
		p1Verts[i] = plyrVerts[i];
		p2Verts[i] = plyrVerts[i];
	}


	// Create new player meshes from the data
	p1Mesh = std::make_shared<Mesh>(p1Verts, plyrVertsTotal, plyrInds, plyrIndsTotal);
	p2Mesh = std::make_shared<Mesh>(p2Verts, plyrVertsTotal, plyrInds, plyrIndsTotal);

	// MESH 4 (PUCK/BALL)
	 // using the sphere class to make a mesh since that's the only object class that works for some reason.
	puckMesh = PrimitiveSphere(puckRadius).getMesh();


	// MESHES 5 - 8
	// horizontal (red) wall vertices
	Vertex wallH[8]
	{
		// Position															Color										
		//  x      y	 z													r	 g	   b	 a						
		{{ -wallHSize.x / 2.0F, -wallHSize.y / 2.0F,  wallHSize.z / 2.0F}, { 1.0f, 0.0f, 0.0f, 1.0f }}, // bottom left, front corner
		{{ -wallHSize.x / 2.0F,  wallHSize.y / 2.0F,  wallHSize.z / 2.0F}, { 1.0f, 0.0f, 0.0f, 1.0f }}, // top left, front corner
		{{ -wallHSize.x / 2.0F, -wallHSize.y / 2.0F, -wallHSize.z / 2.0F}, { 1.0f, 0.0f, 0.0f, 1.0f }}, // bottom left, back corner
		{{ -wallHSize.x / 2.0F,  wallHSize.y / 2.0F, -wallHSize.z / 2.0F}, { 1.0f, 0.0f, 0.0f, 1.0f }}, // top left, back corner

		{{ wallHSize.x / 2.0F, -wallHSize.y / 2.0F,  wallHSize.z / 2.0F}, { 1.0f, 0.0f, 0.0f, 1.0f }}, // bottom right, front corner
		{{ wallHSize.x / 2.0F,  wallHSize.y / 2.0F,  wallHSize.z / 2.0F}, { 1.0f, 0.0f, 0.0f, 1.0f }}, // top right, front corner
		{{ wallHSize.x / 2.0F, -wallHSize.y / 2.0F, -wallHSize.z / 2.0F}, { 1.0f, 0.0f, 0.0f, 1.0f }}, // bottom right, back corner
		{{ wallHSize.x / 2.0F,  wallHSize.y / 2.0F, -wallHSize.z / 2.0F}, { 1.0f, 0.0f, 0.0f, 1.0f }} // top right, back corner
	};

	// vertical (grey) wall vertices
	Vertex wallV[8]
	{
		// Position															Color							
		//  x      y	 z													r	 g	   b	 a		   
		{{ -wallVSize.x / 2.0F, -wallVSize.y / 2.0F,  wallVSize.z / 2.0F}, { 0.3f, 0.3f, 0.3f, 1.0f }}, // bottom left, front corner
		{{ -wallVSize.x / 2.0F,  wallVSize.y / 2.0F,  wallVSize.z / 2.0F}, { 0.3f, 0.3f, 0.3f, 1.0f }}, // top left, front corner
		{{ -wallVSize.x / 2.0F, -wallVSize.y / 2.0F, -wallVSize.z / 2.0F}, { 0.3f, 0.3f, 0.3f, 1.0f }}, // bottom left, back corner
		{{ -wallVSize.x / 2.0F,  wallVSize.y / 2.0F, -wallVSize.z / 2.0F}, { 0.3f, 0.3f, 0.3f, 1.0f }}, // top left, back corner

		{{ wallVSize.x / 2.0F, -wallVSize.y / 2.0F,  wallVSize.z / 2.0F}, { 0.3f, 0.3f, 0.3f, 1.0f }}, // bottom right, front corner
		{{ wallVSize.x / 2.0F,  wallVSize.y / 2.0F,  wallVSize.z / 2.0F}, { 0.3f, 0.3f, 0.3f, 1.0f }}, // top right, front corner
		{{ wallVSize.x / 2.0F, -wallVSize.y / 2.0F, -wallVSize.z / 2.0F}, { 0.3f, 0.3f, 0.3f, 1.0f }}, // bottom right, back corner
		{{ wallVSize.x / 2.0F,  wallVSize.y / 2.0F, -wallVSize.z / 2.0F}, { 0.3f, 0.3f, 0.3f, 1.0f }} // top right, back corner
	};

	// meshes for each wall
	wallL = std::make_shared<Mesh>(wallH, cubeVertsCount, cubeInds, cubeIndsCount); // left red wall
	wallR = std::make_shared<Mesh>(wallH, cubeVertsCount, cubeInds, cubeIndsCount); // right red wall
	wallB = std::make_shared<Mesh>(wallV, cubeVertsCount, cubeInds, cubeIndsCount); // bottom grey wall
	wallT = std::make_shared<Mesh>(wallV, cubeVertsCount, cubeInds, cubeIndsCount); // top grey wall

	// MESHES 9 and 10
	// score box starting size
	Vertex scoreBox[8]
	{
		// Position										Color							
		//  x      y	 z								r	 g	   b	 a
		{{ -0.5F / 2.0F, -0.5F / 2.0F,  0.5F / 2.0F}, { 0.0f, 0.0f, 1.0f, 1.0f }}, // bottom left, front corner
		{{ -0.5F / 2.0F,  0.5F / 2.0F,  0.5F / 2.0F}, { 0.0f, 0.0f, 1.0f, 1.0f }}, // top left, front corner
		{{ -0.5F / 2.0F, -0.5F / 2.0F, -0.5F / 2.0F}, { 0.0f, 0.0f, 1.0f, 1.0f }}, // bottom left, back corner
		{{ -0.5F / 2.0F,  0.5F / 2.0F, -0.5F / 2.0F}, { 0.0f, 0.0f, 1.0f, 1.0f }}, // top left, back corner

		{{ 0.5F / 2.0F, -0.5F / 2.0F,  0.5F / 2.0F}, { 0.0f, 0.0f, 1.0f, 1.0f }}, // bottom right, front corner
		{{ 0.5F / 2.0F,  0.5F / 2.0F,  0.5F / 2.0F}, { 0.0f, 0.0f, 1.0f, 1.0f }}, // top right, front corner
		{{ 0.5F / 2.0F, -0.5F / 2.0F, -0.5F / 2.0F}, { 0.0f, 0.0f, 1.0f, 1.0f }}, // bottom right, back corner
		{{ 0.5F / 2.0F,  0.5F / 2.0F, -0.5F / 2.0F}, { 0.0f, 0.0f, 1.0f, 1.0f }} // top right, back corner
	};

	// creates the meshes for both score boxes
	p1ScoreMesh = std::make_shared<Mesh>(scoreBox, cubeVertsCount, cubeInds, cubeIndsCount);
	p2ScoreMesh = std::make_shared<Mesh>(scoreBox, cubeVertsCount, cubeInds, cubeIndsCount);

	// SHADER
	Shader::Sptr phong = std::make_shared<Shader>();
	phong->Load("lighting.vs.glsl", "blinn-phong.fs.glsl");


	Material::Sptr testMat = std::make_shared<Material>(phong);
	testMat->Set("a_LightPos", { 0, 0, 0.5 });
	testMat->Set("a_LightColor", { 1.0f, 1.0f, 0 });
	testMat->Set("a_AmbientColor", { 1.0f, 1.0f, 1.0f });
	testMat->Set("a_AmbientPower", 0.8f); // change this to change the main lighting power (originally value of 0.1F)
	testMat->Set("a_LightSpecPower", 0.8f);
	testMat->Set("a_LightShininess", 256);
	testMat->Set("a_LightAttenuation", 1.0f);

	SceneManager::RegisterScene("Scene 1");
	SceneManager::RegisterScene("Scene 2");
	SceneManager::SetCurrentScene("Scene 1");

	// update functions and scene registers
	{
		// adds an entity to one of the scenes
		auto& ecs = GetRegistry("Scene 1");
		{
			entt::entity e1 = ecs.create();
			MeshRenderer& m1 = ecs.assign<MeshRenderer>(e1);
			m1.Material = testMat;
			m1.Mesh = myMesh;
		}

		// player 1 object
		{
			entt::entity e2 = ecs.create();
			MeshRenderer& m2 = ecs.assign<MeshRenderer>(e2);
			m2.Material = testMat;
			m2.Mesh = p1Mesh;

			// (&) captures everything outside of the scope.
			auto rotate = [&](entt::entity e, float dt) {
				glm::vec3 translate(0.0F, 0.0F, 0.0F); // translation vector

				// movement increment
				if (p1Ctrls[0]) // moving up
				{
					p1MoveBuildup += pMoveInc.x * dt; // adds to player speed buildup
					translate.x = pMoveInc.x * dt; // translation
				}
				else if (p1Ctrls[1]) // moving down
				{
					p1MoveBuildup = pMoveInc.x * dt; // adds to player speed buildup
					translate.x = -pMoveInc.x * dt; // translation
				}
				else
				{
					p1MoveBuildup = 0.0F; // zeroes out the buildup if the paddle isn't moving.
				}

				auto& transform = CurrentRegistry().get_or_assign<TempTransform>(e);
				transform.Position = p1Pos; // gets the current position
				transform.Position += translate; // translates the mesh
				
				// movement limiters 
				if (transform.Position.x < -pLimit) // too far down
					transform.Position.x = -pLimit;

				else if (transform.Position.x > pLimit) // too far up
					transform.Position.x = pLimit;

				p1Pos = transform.Position; // saving the position
			};
			auto& up = ecs.get_or_assign<UpdateBehaviour>(e2);
			up.Function = rotate;
		}

		// player 2 object
		{
			entt::entity e3 = ecs.create();
			MeshRenderer& m3 = ecs.assign<MeshRenderer>(e3);
			m3.Material = testMat;
			m3.Mesh = p2Mesh;

			// (&) captures everything outside of the scope.
			auto rotate = [&](entt::entity e, float dt) {
				glm::vec3 translate(0.0F, 0.0F, 0.0F); // translation vector

				// movement increment
				if (p2Ctrls[0]) // move up
				{
					p2MoveBuildup += pMoveInc.x * dt; // saves player buildup
					translate.x = pMoveInc.x * dt; // translation
				}
				else if (p2Ctrls[1]) // move down
				{
					p2MoveBuildup += pMoveInc.x * dt; // saves player buildup
					translate.x = -pMoveInc.x * dt; // translation
				}
				else
				{
					p2MoveBuildup = 0.0F; // zeroes out buildup if the paddle has stopped moving.
				}

				auto& transform = CurrentRegistry().get_or_assign<TempTransform>(e);
				transform.Position = p2Pos;

				transform.Position += translate; // translates the mesh

				// movement limiters
				if (transform.Position.x < -pLimit) // too far down
					transform.Position.x = -pLimit;
				
				else if (transform.Position.x > pLimit) // too far up
					transform.Position.x = pLimit;
				

				p2Pos = transform.Position; // saving position
			};
			auto& up = ecs.get_or_assign<UpdateBehaviour>(e3);
			up.Function = rotate;
		}

		// puck object
		{
			entt::entity e4 = ecs.create();
			MeshRenderer& m4 = ecs.assign<MeshRenderer>(e4);
			m4.Material = testMat;
			m4.Mesh = puckMesh;

			// (&) captures everything outside of the scope.
			auto rotate = [&](entt::entity e, float dt) {
				
				glm::vec3 translate(0.0F, 0.0F, 0.0F); // translation vector
				float bounds = 15.0F; // bounds for respawning the puck if it goes out of the game space

				translate += puckMoveInc * dt; // puck movement

				// adding the puck buildup to the puck's current movement to increase its speed.
				translate.y += dt * (puckMoveInc.y > 0.0F) ? puckMoveBuildup : -puckMoveBuildup;

				// sets the puck movement buildup to 0 if it's too low, since a division operation cannot result in a 0.
				if (puckMoveBuildup <= 0.001F)
					puckMoveBuildup = 0.0F;
				else
				{
					// dimishing the puck movement buildup
					puckMoveBuildup *= 0.8F;
				}
				
				auto& transform = CurrentRegistry().get_or_assign<TempTransform>(e);
				
				transform.Position = puckPos; // getting the puck's current position

				// resetting the position of the puck if it goes too far outside the game space.
				if (transform.Position.x > bounds || transform.Position.x < -bounds) // x-axis
				{
					transform.Position = glm::vec3();
					puckMoveBuildup = 0.0F;
				}

				if (transform.Position.y > bounds || transform.Position.y < -bounds) // y-axis
				{
					transform.Position = glm::vec3();
					puckMoveBuildup = 0.0F;
				}

				if (transform.Position.z > bounds || transform.Position.z < -bounds) // z-axis
				{
					transform.Position = glm::vec3();
					puckMoveBuildup = 0.0F;
				}

				transform.Position += translate; // translates the mesh

				puckPos = transform.Position; // saving puck position
			};
			auto& up = ecs.get_or_assign<UpdateBehaviour>(e4);
			up.Function = rotate;
		}

		// walls don't move, so nothing really happens here on that front.
		// wall (left) object
		{
			entt::entity e5 = ecs.create();
			MeshRenderer& m5 = ecs.assign<MeshRenderer>(e5);
			m5.Material = testMat;
			m5.Mesh = wallL;

			// (&) captures everything outside of the scope.
			auto rotate = [&](entt::entity e, float dt) {

				glm::vec3 translate(0.0F, 0.0F, 0.0F); // translation vector


				auto& transform = CurrentRegistry().get_or_assign<TempTransform>(e);

				transform.Position = wallLPos; // getting current position

				wallLPos = transform.Position; // saving new position

			};
			auto& up = ecs.get_or_assign<UpdateBehaviour>(e5);
			up.Function = rotate;
		}

		// wall (right) object
		{
			entt::entity e6 = ecs.create();
			MeshRenderer& m6 = ecs.assign<MeshRenderer>(e6);
			m6.Material = testMat;
			m6.Mesh = wallR;

			// (&) captures everything outside of the scope.
			auto rotate = [&](entt::entity e, float dt) {

				glm::vec3 translate(0.0F, 0.0F, 0.0F); // translation vector


				auto& transform = CurrentRegistry().get_or_assign<TempTransform>(e);

				transform.Position = wallRPos; // getting position

				wallRPos = transform.Position; // saving position

			};
			auto& up = ecs.get_or_assign<UpdateBehaviour>(e6);
			up.Function = rotate;
		}

		// wall (up) object
		{
			entt::entity e7 = ecs.create();
			MeshRenderer& m7 = ecs.assign<MeshRenderer>(e7);
			m7.Material = testMat;
			m7.Mesh = wallT;

			// (&) captures everything outside of the scope.
			auto rotate = [&](entt::entity e, float dt) {

				glm::vec3 translate(0.0F, 0.0F, 0.0F); // translation vector


				auto& transform = CurrentRegistry().get_or_assign<TempTransform>(e);

				transform.Position = wallTPos; // getting position

				wallTPos = transform.Position; // saving position
			};
			auto& up = ecs.get_or_assign<UpdateBehaviour>(e7);
			up.Function = rotate;
		}

		// wall (down) object
		{
			entt::entity e8 = ecs.create();
			MeshRenderer& m8 = ecs.assign<MeshRenderer>(e8);
			m8.Material = testMat;
			m8.Mesh = wallB;

			// (&) captures everything outside of the scope.
			auto rotate = [&](entt::entity e, float dt) {

				glm::vec3 translate(0.0F, 0.0F, 0.0F); // translation vector


				auto& transform = CurrentRegistry().get_or_assign<TempTransform>(e);

				transform.Position = wallBPos; // getting position

				wallBPos = transform.Position; // saving position
			};
			auto& up = ecs.get_or_assign<UpdateBehaviour>(e8);
			up.Function = rotate;
		}


		// Player 1 Score Box
		{
			entt::entity e9 = ecs.create();
			MeshRenderer& m9 = ecs.assign<MeshRenderer>(e9);
			m9.Material = testMat;
			m9.Mesh = p1ScoreMesh;

			// (&) captures everything outside of the scope.
			auto rotate = [&](entt::entity e, float dt) {

				glm::vec3 translate(0.0F, 0.0F, 0.0F); // translation vector


				auto& transform = CurrentRegistry().get_or_assign<TempTransform>(e);

				transform.Position = p1ScorePos; // getting current position

				p1ScoreScale = { 0.01F + 1.0F * p1Score, 1.0F, 1.0F }; // scalaing the box proportional to the player's score
				transform.Scale = p1ScoreScale; // saving new scale

				p1ScorePos = transform.Position; // saving position
			};
			auto& up = ecs.get_or_assign<UpdateBehaviour>(e9);
			up.Function = rotate;
		}


		// Player 2 Score Box
		{
			entt::entity e10 = ecs.create();
			MeshRenderer& m10 = ecs.assign<MeshRenderer>(e10);
			m10.Material = testMat;
			m10.Mesh = p2ScoreMesh;

			// (&) captures everything outside of the scope.
			auto rotate = [&](entt::entity e, float dt) {

				glm::vec3 translate(0.0F, 0.0F, 0.0F); // translation vector


				auto& transform = CurrentRegistry().get_or_assign<TempTransform>(e);

				transform.Position = p2ScorePos; // getting position

				p2ScoreScale = { 0.01F + 1.0F * p2Score, 1.0F, 1.0F }; // scaling proportional to player 2's current score
				transform.Scale = p2ScoreScale; // setting the scale

				p2ScorePos = transform.Position; // saving position
			};
			auto& up = ecs.get_or_assign<UpdateBehaviour>(e10);
			up.Function = rotate;
		}
	}

	// Create and compile shader
	myShader = std::make_shared<Shader>();
	// myShader->Compile(vs_source, fs_source); // no longer needed since we have a dedicated file.
	myShader->Load("passthrough.vert.glsl", "passthrough.frag.glsl");
	
	myModelTransform = glm::mat4(1.0f);
}

void Game::UnloadContent() {
}

void Game::Update(float deltaTime) {

	// if 'c' is pressed, the camera mode is changed. This is just a holdover from the original version of this code, and shouldn't be used.
	static bool isKeyDown = false; // makes it so that the key isn't constantly detected if it is released.
	if (glfwGetKey(myWindow, GLFW_KEY_C) == GLFW_PRESS)
	{
		if (!isKeyDown) {
			perspectiveCamera = !perspectiveCamera;

			// perpsective (fovRadians, aspect, zNear, zFar
			// ortho (left, right, bottom, top, near, far)

			// switches camera mode
			myCamera->Projection = (perspectiveCamera) ?
				glm::perspective(cameraAngle, 1.0f, 0.01f, 1000.0f) : glm::ortho(-5.0f, 5.0f, -5.0f, 5.0f, 0.0f, 100.0f);
		}
		isKeyDown = true;
	}
	else {
		isKeyDown = false;
	}

	// calling all of our functions for our update behaviours.
	auto view = CurrentRegistry().view<UpdateBehaviour>();
	for (const auto& e : view) {
		auto& func = CurrentRegistry().get<UpdateBehaviour>(e);
		if (func.Function) {
			func.Function(e, deltaTime);
		}
	}

	Collision(); // checks for collision.
}

void Game::InitImGui() {
	// Creates a new ImGUI context
	ImGui::CreateContext();
	// Gets our ImGUI input/output
	ImGuiIO& io = ImGui::GetIO();
	// Enable keyboard navigation
	io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
	// Allow docking to our window
	io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;
	// Allow multiple viewports (so we can drag ImGui off our window)
	io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;
	// Allow our viewports to use transparent backbuffers
	io.ConfigFlags |= ImGuiConfigFlags_TransparentBackbuffers;
	// Set up the ImGui implementation for OpenGL
	ImGui_ImplGlfw_InitForOpenGL(myWindow, true);
	ImGui_ImplOpenGL3_Init("#version 410");

	// Dark mode FTW
	ImGui::StyleColorsDark();
	// Get our imgui style
	ImGuiStyle& style = ImGui::GetStyle();
	//style.Alpha = 1.0f;
	if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable) {
		style.WindowRounding = 0.0f;
		style.Colors[ImGuiCol_WindowBg].w = 0.8f;
	}
}

void Game::ShutdownImGui() {
	// Cleanup the ImGui implementation
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	// Destroy our ImGui context
	ImGui::DestroyContext();
}

void Game::ImGuiNewFrame() {
	// Implementation new frame
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplGlfw_NewFrame();
	// ImGui context new frame
	ImGui::NewFrame();
}

void Game::ImGuiEndFrame() {
	// Make sure ImGui knows how big our window is
	ImGuiIO& io = ImGui::GetIO();
	int width{ 0 }, height{ 0 };
	glfwGetWindowSize(myWindow, &width, &height);
	io.DisplaySize = ImVec2(width, height);
	// Render all of our ImGui elements
	ImGui::Render();
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
	// If we have multiple viewports enabled (can drag into a new window)
	if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable) {
		// Update the windows that ImGui is using
		ImGui::UpdatePlatformWindows();
		ImGui::RenderPlatformWindowsDefault();
		// Restore our gl context
		glfwMakeContextCurrent(myWindow);
	}
}

void Game::Run()
{
	Initialize();
	InitImGui();
	LoadContent();
	static float prevFrame = glfwGetTime();
	// Run as long as the window is open
	while (!glfwWindowShouldClose(myWindow)) {
		// Poll for events from windows
		// clicks, key presses, closing, all that
		glfwPollEvents();
		float thisFrame = glfwGetTime();
		float deltaTime = thisFrame - prevFrame;
		Update(deltaTime);
		Draw(deltaTime);
		ImGuiNewFrame();
		DrawGui(deltaTime);
		ImGuiEndFrame();
		prevFrame = thisFrame;
		// Present our image to windows
		glfwSwapBuffers(myWindow);

		// added so that the previous frame is updated. Otherwise, it would just be since the beginning of the program.
		prevFrame = thisFrame;
	}
	UnloadContent();
	ShutdownImGui();
	Shutdown();
}

// resizes the window and keeps size proportionate.
void Game::Resize(int newWidth, int newHeight)
{
	// set to float since we're calculating the new projecction as the screen size.
	if(perspectiveCamera) // camera is in perspective mode
	{
		myCamera->Projection = glm::perspective(cameraAngle, newWidth / (float)newHeight, 0.01f, 1000.0f);
	}
	else // camera is in orthographic mode
	{
		myCamera->Projection = glm::ortho(-5.0f * newWidth / (float)newHeight, 5.0f * newWidth / (float)newHeight, -5.0f, 5.0f, 0.0f, 100.0f);
	}
	
}

// called when a mouse button has been pressed
void Game::MouseButtonPressed(GLFWwindow* window, int button) {
	Game* game = (Game*)glfwGetWindowUserPointer(window);

	if (game == nullptr) // if game is 'null', then nothing happens
		return;

	// checks each button
	switch (button) {
	case GLFW_MOUSE_BUTTON_LEFT:
		break;
	case GLFW_MOUSE_BUTTON_MIDDLE:
		break;
	case GLFW_MOUSE_BUTTON_RIGHT:
		break;
	}
}

// called when a mouse button has been pressed
void Game::MouseButtonReleased(GLFWwindow* window, int button) {
	Game* game = (Game*)glfwGetWindowUserPointer(window);

	if (game == nullptr) // if game is 'null', then nothing happens
		return;

	// checks each button
	switch (button) {
	case GLFW_MOUSE_BUTTON_LEFT:
		break;
	case GLFW_MOUSE_BUTTON_MIDDLE:
		break;
	case GLFW_MOUSE_BUTTON_RIGHT:
		break;
	}
}

// key functions, which are all used for movement. The left and right keys are checked, but ultimately unused.
// key has been pressed
void Game::KeyPressed(GLFWwindow* window, int key)
{
	Game* game = (Game*)glfwGetWindowUserPointer(window);

	if (game == nullptr) // if game is 'null', then nothing happens
		return;

	// checks key value.
	switch (key)
	{
		// PLAYER 1
	case GLFW_KEY_W: // up
		p1Ctrls[0] = true;
		break;
	case GLFW_KEY_S: // down
		p1Ctrls[1] = true;
		break;
	case GLFW_KEY_A: // left
		p1Ctrls[2] = true;
		break;
	case GLFW_KEY_D: // right
		p1Ctrls[3] = true;
		break;

		// PLAYER 2
	case GLFW_KEY_UP: // up
		p2Ctrls[0] = true;
		break;
	case GLFW_KEY_DOWN: // down
		p2Ctrls[1] = true;
		break;
	case GLFW_KEY_LEFT: // left
		p2Ctrls[2] = true;
		break;
	case GLFW_KEY_RIGHT: // right
		p2Ctrls[3] = true;
		break;
	}
}

// key is being held
void Game::KeyHeld(GLFWwindow* window, int key)
{
	Game* game = (Game*)glfwGetWindowUserPointer(window);

	if (game == nullptr) // if game is 'null', then it is returned
		return;

	switch (key)
	{
		// PLAYER 1
	case GLFW_KEY_W: // up
		p1Ctrls[0] = true;
		break;
	case GLFW_KEY_S: // down
		p1Ctrls[1] = true;
		break;
	case GLFW_KEY_A: // left
		p1Ctrls[2] = true;
		break;
	case GLFW_KEY_D: // right
		p1Ctrls[3] = true;
		break;

		// PLAYER 2
	case GLFW_KEY_UP: // up
		p2Ctrls[0] = true;
		break;
	case GLFW_KEY_DOWN: // down
		p2Ctrls[1] = true;
		break;
	case GLFW_KEY_LEFT: // left
		p2Ctrls[2] = true;
		break;
	case GLFW_KEY_RIGHT: // right
		p2Ctrls[3] = true;
		break;
	}
}

// key hs been released
void Game::KeyReleased(GLFWwindow* window, int key)
{
	Game* game = (Game*)glfwGetWindowUserPointer(window);

	if (game == nullptr) // if game is 'null', then it is returned
		return;

	switch (key)
	{
		// PLAYER 1
	case GLFW_KEY_W: // up
		p1Ctrls[0] = false;
		break;
	case GLFW_KEY_S: // down
		p1Ctrls[1] = false;
		break;
	case GLFW_KEY_A: // left
		p1Ctrls[2] = false;
		break;
	case GLFW_KEY_D: // right
		p1Ctrls[3] = false;
		break;

		// PLAYER 2
	case GLFW_KEY_UP: // up
		p2Ctrls[0] = false;
		break;
	case GLFW_KEY_DOWN: // down
		p2Ctrls[1] = false;
		break;
	case GLFW_KEY_LEFT: // left
		p2Ctrls[2] = false;
		break;
	case GLFW_KEY_RIGHT: // right
		p2Ctrls[3] = false;
		break;
	}
}


void Game::Draw(float deltaTime) {
	// Clear our screen every frame
	glClearColor(myClearColor.x, myClearColor.y, myClearColor.z, myClearColor.w);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	// We'll grab a reference to the ecs to make things easier
	auto& ecs = CurrentRegistry();
	// We sort our mesh renderers based on material properties
	// This will group all of our meshes based on shader first, then material second
	ecs.sort<MeshRenderer>([](const MeshRenderer& lhs, const MeshRenderer& rhs) {
		if (rhs.Material == nullptr || rhs.Mesh == nullptr)
			return false;
		else if (lhs.Material == nullptr || lhs.Mesh == nullptr)
			return true;
		else if (lhs.Material->GetShader() != rhs.Material->GetShader())
			return lhs.Material->GetShader() < rhs.Material->GetShader();
		else
			return lhs.Material < rhs.Material;
		});

	// These will keep track of the current shader and material that we have bound
	Material::Sptr mat = nullptr;
	Shader::Sptr boundShader = nullptr;
	// A view will let us iterate over all of our entities that have the given component types
	auto view = ecs.view<MeshRenderer>();

	for (const auto& entity : view) {
		// Get our shader
		const MeshRenderer& renderer = ecs.get<MeshRenderer>(entity);
		// Early bail if mesh is invalid
		if (renderer.Mesh == nullptr || renderer.Material == nullptr)
			continue;
		// If our shader has changed, we need to bind it and update our frame-level uniforms
		if (renderer.Material->GetShader() != boundShader) {
			boundShader = renderer.Material->GetShader();
			boundShader->Bind();
			boundShader->SetUniform("a_CameraPos", myCamera->GetPosition());
		}
		// If our material has changed, we need to apply it to the shader
		if (renderer.Material != mat) {
			mat = renderer.Material;
			mat->Apply();
		}

		// We'll need some info about the entities position in the world
		const TempTransform& transform = ecs.get_or_assign<TempTransform>(entity);
		// Get the object's transformation
		glm::mat4 worldTransform = transform.GetWorldTransform();
		// Our normal matrix is the inverse-transpose of our object's world rotation
		// Recall that everything's backwards in GLM
		glm::mat3 normalMatrix = glm::mat3(glm::transpose(glm::inverse(worldTransform)));

		// Update the MVP using the item's transform
		mat->GetShader()->SetUniform(
			"a_ModelViewProjection",
			myCamera->GetViewProjection() *
			worldTransform);
		// Update the model matrix to the item's world transform
		mat->GetShader()->SetUniform("a_Model", worldTransform);
		// Update the model matrix to the item's world transform
		mat->GetShader()->SetUniform("a_NormalMatrix", normalMatrix);
		// Draw the item
		renderer.Mesh->Draw();

	}

}

void Game::DrawGui(float deltaTime) {
	// The DrawGui window isn't used.

	// Open a new ImGui window
	ImGui::Begin("Colour Picker");
	
	// Draw widgets here
	// ImGui::SliderFloat4("Color", &myClearColor.x, 0, 1); // Original
	ImGui::ColorPicker4("Color", &myClearColor.x); // new version
	if (ImGui::InputText("Title", myWindowTitle, 31))
	{
		glfwSetWindowTitle(myWindow, myWindowTitle);
	}

	if (ImGui::Button("Apply")) // adding another button, which allows for the application of the window title.
	{
		glfwSetWindowTitle(myWindow, myWindowTitle);
	}

	ImGui::Text("C: Change Camera Mode");
	// ImGui::Text(("Time: " + std::to_string(glfwGetTime())).c_str()); // requires inclusion of <string>

	// draws a button for each scene name.
	auto it = SceneManager::Each();
	for (auto& kvp : it) {
		if (ImGui::Button(kvp.first.c_str())) {
			SceneManager::SetCurrentScene(kvp.first);
		}
	}

	ImGui::End();
}

// collision calculations
bool Game::Collision()
{
	bool col = false; // saves the result of the collision check.

	// player 1 collision with the puck
	col = util::math::aabbCollision(
		util::math::Vec3(p1Pos.x, p1Pos.y, p1Pos.z), pWidth, pHeight, pDepth,
		util::math::Vec3(puckPos.x, puckPos.y, puckPos.z), puckRadius, puckRadius, puckRadius
	);

	// if col is equal to 'true', then player 1 has hit the puck.
	if (col)
	{
		puckMoveInc.y *= -1; // reversing y direction
		lastHit = 1; // saves that player 1 hit the ball last
		puckMoveBuildup = p1MoveBuildup * 0.3F; // builds up the puck speed
		
		// possibly changes the direction of the puck on the x-axis based on where it collided with the paddle.
		puckMoveInc.x = (puckPos.x > p1Pos.x) ? abs(puckMoveInc.x) : -abs(puckMoveInc.x);
		
		return col;
	}

	// player 2 collision with the puck
	col = util::math::aabbCollision(
		util::math::Vec3(p2Pos.x, p2Pos.y, p2Pos.z), pWidth, pHeight, pDepth,
		util::math::Vec3(puckPos.x, puckPos.y, puckPos.z), puckRadius, puckRadius, puckRadius
	);

	// collision is equal to 'true'
	if (col)
	{
		puckMoveInc.y *= -1; // reversing y-direction movement
		lastHit = 2; // player 2 hit the puck last
		puckMoveBuildup = p2MoveBuildup * 0.3F; // builds up the puck speed

		// possibly changes the direction of the puck on the x-axis based on where it collided with the paddle.
		puckMoveInc.x = (puckPos.x > p2Pos.x) ? abs(puckMoveInc.x) : -abs(puckMoveInc.x);

		return col;
	}

	// puck wall collision (grey walls)
	// Top Wall
	col = util::math::aabbCollision(
		util::math::Vec3(wallTPos.x, wallTPos.y, wallTPos.z), wallVSize.x, wallVSize.y, wallVSize.z,
		util::math::Vec3(puckPos.x, puckPos.y, puckPos.z), puckRadius, puckRadius, puckRadius
	);

	// collision is equal to 'true'
	if (col)
	{
		// reverses direction on hte x-axis
		puckMoveInc.x *= -1;
		return col;
	}

	// Bottom Wall
	col = util::math::aabbCollision(
		util::math::Vec3(wallBPos.x, wallBPos.y, wallBPos.z), wallVSize.x, wallVSize.y, wallVSize.z,
		util::math::Vec3(puckPos.x, puckPos.y, puckPos.z), puckRadius, puckRadius, puckRadius
	);

	// collision is equal to 'true'
	if (col)
	{
		// reveres direction on the x-axis
		puckMoveInc.x *= -1;
		return col;
	}
	
	
	// puck goal collision (red wall)
	// Left Wall
	col = util::math::aabbCollision(
		util::math::Vec3(wallLPos.x, wallLPos.y, wallLPos.z), wallHSize.x, wallHSize.y, wallHSize.z,
		util::math::Vec3(puckPos.x, puckPos.y, puckPos.z), puckRadius, puckRadius, puckRadius
	);

	// collision is equal to 'true'
	if (col)
	{
		p1Score++; //  player 1 has scored on player 2.
		puckMoveInc.y *= -1; // reverses puck direction for respawn

		puckPos = p2Pos + glm::vec3(0.0F, 1.0F, 0.0F); // puck starting position; places the puck in front of player 2
		return col;
	}

	// Right Wall
	col = util::math::aabbCollision(
		util::math::Vec3(wallRPos.x, wallRPos.y, wallRPos.z), wallHSize.x, wallHSize.y, wallHSize.z,
		util::math::Vec3(puckPos.x, puckPos.y, puckPos.z), puckRadius, puckRadius, puckRadius
	);

	// collision is equal to 'true'; scored on player 1
	if (col)
	{
		p2Score++; // player 2 has scored on player 1
		puckMoveInc.y *= -1; // reveres puck movement for respawn
		puckPos = p1Pos + glm::vec3(0.0F, -1.0F, 0.0F); // places puck in front of player 1.
		
		return col;
	}
	
	// no collisions at all
	return false;
}
