/*
 * Name: Roderick "R.J." Montague
 * Student Number: 100701758
 * Date: 11/04/2019
 * Description: the main class that connects all of the graphics functions together to make the game work.
*/

// GAME (HEADER)
#pragma once
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "GLM/glm.hpp"

#include "Shader.h"
#include "Mesh.h"
#include "Camera.h"

#include "objects/PrimitivePlane.h"
#include "objects/PrimitiveCube.h"
#include "objects/PrimitiveSphere.h"

#include <vector>
#include <iostream>

class Game {
public:
	Game();
	
	~Game();
	
	void Run();

	void Resize(int newWidth, int newHeight);

	// called when a mouse button has been pressed
	void MouseButtonPressed(GLFWwindow* window, int button);

	// called when a mouse button has been released
	void MouseButtonReleased(GLFWwindow* window, int button);

	// called when a key has been pressed
	virtual void KeyPressed(GLFWwindow* window, int key);

	// called when a key is being held down
	virtual void KeyHeld(GLFWwindow* window, int key);

	// called when a key has been released
	virtual void KeyReleased(GLFWwindow* window, int key);

protected:
	void Initialize();
	
	void Shutdown();
	
	void LoadContent();
	
	void UnloadContent();
	
	void InitImGui();
	
	void ShutdownImGui();
	
	void ImGuiNewFrame();
	
	void ImGuiEndFrame();
	
	void Update(float deltaTime);
	
	void Draw(float deltaTime);
	
	void DrawGui(float deltaTime);

private:
	// called to calculate collision for obects in the game.
	bool Collision();

	// Stores the main window that the game is running in
	GLFWwindow* myWindow;

	// Stores the clear color of the game's window
	glm::vec4 myClearColor;
	
	// Stores the title of the game's window
	char myWindowTitle[32];

	// A shared pointer to the rainbow plane mesh.
	Mesh::Sptr myMesh;
	
	// player 1's mesh
	Mesh::Sptr p1Mesh;

	// player 2's mesh
	Mesh::Sptr p2Mesh;

	// the puck's mesh
	Mesh::Sptr puckMesh;

	// A shared pointer to our shader
	Shader::Sptr myShader;

	// An  extra mesh held over from past tutorials; unused.
	// Mesh::Sptr myMesh2;
	// Shader::Sptr myShader2; // not needed

	// a vector of objects; unused
	std::vector<Object *> objects;

	// camera
	Camera::Sptr myCamera;
	bool perspectiveCamera = false; // if 'true', the perpsective camera is used. If 'false', orthographic camera is used.
	float cameraAngle = 60.0F; // the angle of the camera

	// Our models transformation matrix
	glm::mat4 myModelTransform;

	// PLAYER VARIABLES
	// Player Controls
	// [0] = UP, [1] = DOWN, [2] = LEFT, [3] = RIGHT
	bool p1Ctrls[4] = { false, false, false, false }; // player 1 controls

	bool p2Ctrls[4] = { false, false, false, false }; // player 2 cntrols

	// when the puck hits the paddle, it gets a boost in speed if the paddle was moving at the time.
	// the amount of boost depends on how long the paddle has been moving for, which is tracked here.
	// this isn't a buildup of time, but rather an accumulation based on the paddle's current position in reference to where it started.
	// the value that gets accumulated is the movement increment applied to the paddles to make them move.
	float p1MoveBuildup = 0.0F; // speed buildup from player 1's movement
	float p2MoveBuildup = 0.0F; // speed buildup from player 2's movement

	// used to move the players. Both players naturally move at the same speed.
	glm::vec3 pMoveInc = { 10.0f, 0.0F, 0.0F };

	// player positions
	glm::vec3 p1Pos = { 0.0f, 10.0F, 0.0F }; // player 1's position
	glm::vec3 p2Pos = { 0.0f, -10.0F, 0.0F }; // player 2's position
	
	// limits how far the paddles can move in both the positive and negative directions on the x-axis.
	// currently, the paddle position range on the x-axis is [-9.2, 9.2].
	float pLimit = 9.2F;

	// saves what player hit the puck last.
	// * 0 = neither
	// * 1 = player 1
	// * 2 = player 2 
	unsigned short int lastHit = 0;
	
	// the size of the player paddles. 
	// the player paddles are thin so that the puck has a lower chance of getting stuck inside them.
	float pWidth = 1.0F; // width of players
	float pHeight = 0.1F; // height of players
	float pDepth = 1.0F; // depth of players


	// PUCK VARIABLES
	glm::vec3 puckPos = { 0.0f, 0.0F, 0.0F }; // puck's position
	float puckRadius = 0.5F; // puck's radius (which is ultimately used as the side length in AABB collision)

	// movement increment for the puck. 
	// these get changed from positive to negative (and vice versa) based on what direction the puck should go.
	glm::vec3 puckMoveInc = { -6.0f, -6.0f, 0.0f };

	// puck buildup, which gets values from the player buildup to boost the puck's speed.
	float puckMoveBuildup = 0.0F;

	// WALL VARIABLES
	// the size of the horizontal walls and verticle walls. 
	// Keep in mind that the camera is upside down and tilted, so that may make the variable names seem flipped.
	 // glm::vec3(width, height, and depth)
	glm::vec3 wallHSize{ 20.0F, 1.0F, 1.0F }; // red walls (goals)
	glm::vec3 wallVSize{ 1.0F, 24.8F, 1.0F }; // grey walls

	// the four surrounding wall.
	Mesh::Sptr wallL; // left wall mesh
	glm::vec3 wallLPos{ 0.0f, -12.5f, 0.0f }; // left red wall position

	Mesh::Sptr wallR; // right wall mesh
	glm::vec3 wallRPos{ 0.0f, 12.5f, 0.0f }; // right red wall position

	Mesh::Sptr wallT; // top wall mesh
	glm::vec3 wallTPos{ -10.0f, 0.0f, 0.0f }; // top grey wall position

	Mesh::Sptr wallB; // bottom wall mesh
	glm::vec3 wallBPos{ 10.0f, 0.0f, 0.0f }; // bottom grey wall position


	// SCORE BOX
	glm::vec3 scoreBox{ 1.0F, 1.0F, 1.0F }; // score box size (width, height, depth)

	Mesh::Sptr p1ScoreMesh; // player 1 score box mesh
	glm::vec3 p1ScorePos{0.0f, 15.0f, 0.0f}; // p1 score box position
	glm::vec3 p1ScoreScale{ 1.0F, 1.0F, 1.0F }; // p1 score box scale
	float p1Score = 0.0F; // player 1's score

	Mesh::Sptr p2ScoreMesh; // player 2 score box mesh
	glm::vec3 p2ScorePos{ 0.0f, -15.0f, 0.0f }; // p2 score box position
	glm::vec3 p2ScoreScale{ 1.0F, 1.0F, 1.0F }; // p2 score box scale
	float p2Score = 0.0f; // player 2's score
};