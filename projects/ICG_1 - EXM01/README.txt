INTRODUCTION TO COMPUTER GRAPHICS (INFR 1350U) - EXAM README (Amended)
----------------------------------------------------------------------------

Name: Roderick "R.J." Montague
	- I worked alone for this practical exam, but much of my code is reused or otherwise based on.
Student Number: 100701758
Date: 11/04/2019
Game: 1) Air Hockey

DISCLAIMERS:
This project contains files that are adapted from my GDW team's engine, Teacher's Assistant Matthew's framework, and past personal projects of mine.
	- The files in the 'src/objects' folder path come from my object loader, and my GDW team's engine.
		- I don't know if any of them used files from our engine, but in case they did, here are their names and student numbers:
			* Jonah Griffin (100702748)  
			* Kennedy Adams (100632983)
			* Nathan Tuck (100708651)  
			* Ryan Burton (100707511)    
			* Spencer Tester (100653129)
			* Stephane "Steph" Gagnon (100694227)  

	
	- The files in the 'src/utils' folder path come from a personal project of mine that's used to store various utility functions (e.g. rotations, degree to radian conversions, etc.).
		- I've built this up over the course of 1st year and 2nd year, and have used versions of these files before (I have evidence of such if needed).
			- e.g. my GDW game for 1st year's winter semester
		- So if any plagiarism flags come up, just know that it's probably just from what I've done with these files in the past.
	- The files outside of both folders (i.e. 'src/' file path) are adapations of what we've been doing in the Computer Graphics tutorials.


IMPORTANT INFORMATION:
* I used the Computer Graphics framework, so the project must be sloted in there in order for it to work.
* I included a ton of files that I didn't need to use. Said files were brought in either because they came with other files I did need, or because I wanted to use them but couldn't.
* As such, you can really just focus on Game.h/Game.cpp, but if you're curious about what other files are used, I'll mention them below.
* The files of interest are as follows:
	- Game.h and Game.cpp (game code)
	- PrimitiveSphere.h and PrimitiveSphere.cpp (used for creation of puck mesh)
	- utils/math/Collision.h and utils/math/Collision.cpp (uses exclusively the AABB function for the collision)
	- utils/math/Rotation.h and utils/math/Rotation.cpp (originally used to setup normals, but they ultimately didn't get used)
	- Any other files that were used in the tutorial framework up to this point.
* The camera is upside down and angled strangely. As such, some of the object names and position changes may not look consistent with what actually happens on screen.
	- It's for this reason that up seems to be down, and down seems to be up when it comes to movement on screen.


REQUIREMENTS:
* Two-player game (playing on one keyboard)
	- I have two players, one of which uses WASD, while the other uses the arrow keys.
* puck bounces off table sides and players
	- bounces at an angle proportional to its current angle of movement when collision occurs.
* If puck crosses the goal line, restart the game and place the puck on the side of the player who conceded the goal.
	- goal lines are represented as red rectangular prims. 
	- then the puck hits the goal, it spawns in front of the player that got scored on.
	- the puck then moves away from the player the moment it respawns, starting another round.
	- if the puck goes too far out of the game space and wasn't hit by anyone, it respawns in the middle.

* Puck slows down as it travels on the table
	- the puck gets a boost in speed if a moving paddle hits it.
		- how much of a boost depends on how long the paddle has been moving in a given direction.	
* Puck bounces at different angles depending on where it hits the player or goal corners;
	- as mentioned before, the puck bounces at an angle proportional to the angle it came in at when it comes to the walls.
	- depending on where it hits the given paddle, the ball may start heading in the direction it came from, or just bounce off like normal as if it hit a wall.
* Different forces are applied to the puck (depending on how fast a player hits the puck);
	- as mentioned prior, a force gets applied to the puck if the paddle is moving when it hits it.
	- the amount of force applied is a fraction of the buildup the paddle has from moving so long without stopping.
* Overlay score (can be just dots) 
	- scores are presented as blue rectangles on opposite sides of the game space.
	- the score bars are on the side of the given player they represent.
	- with each goal scored, a given bar gets longer and longer, being stretched out proportional to the aligned player's total score.
