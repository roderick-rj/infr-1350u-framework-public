INTRODUCTION TO COMPUTER GRAPHICS (INFR INFR 1350U) - EXAM README
----------------------------------------------------------------

Name: Roderick "R.J." Montague
	- I worked alone for this practical exam.
Student Number: 100701758
Date: 10/28/2019
Game: 1) Air Hockey

Files:
*I included a ton of files that I didn't need to use. 
*You can really just focus on Game.h/Game.cpp, but if you're curious about what other files are used, see below.
*The files of interest are as follows:
- Game.h (game code)
- Game.cpp (game code)
- PrimitiveSphere.h (used for puck creation)
- PrimitiveSphere.cpp (used for puck creation)
- utils/math/Collision.h (uses the AABB function for the collision)
- utils/math/Collision.cpp (uses the AABB function for the collision)
- utils/math/Rotation.h (used to help set up the normals, which don't actually work properly, so you can ignore this)
- utils/math/Rotation.cpp (used to help set up the normals, which don't actually work properly, so you can ignore this)

Notes:
- used the framework, so it must be sloted in there in order to work.
- I have two players that move using WASD and the arrow keys respectively.
- I have collisions, but because hte positions are set wrong the objects don't move properly.
	- the algorithm does work though.
- If the puck goes too far off-screen, it respawns in the middle.
- the PrimitiveCube and PrimitivePlane files didn't work, but the PrimitiveSphere did... so even though these are all included, all but the sphere can be ignored.
- I apoligize for the messy comments in Game.h and Game.cpp.