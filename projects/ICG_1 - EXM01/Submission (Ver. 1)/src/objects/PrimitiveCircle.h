// Used for making circles
#pragma once

#include "Primitive.h"

typedef class PrimitiveCircle : public Primitive
{
public:
	// takes a position, radius, and vertices. Vertices must be at least edges.
	// at least 3 dges must be provided.
	PrimitiveCircle(float radius = 1.0F, unsigned int edges = 10);
	
	// gets the radius
	float getRadius() const;

	// sets the radius
	void setRadius(float r);
private:
	float radius;

protected:

} Circle;


