#include "PhysicsBody.h"
#include "utils/math/Collision.h"

// sets the physics body ID
PhysicsBody::PhysicsBody(int id) : id(id), position(glm::vec3()) {}

// sets the ID for a specific type of physics body
PhysicsBody::PhysicsBody(int id, glm::vec3 pos) : id(id), position(pos) {}

/*
// gets the ID for the physics body
 *** (0): no specifier
 *** (1): box
 *** (2): sphere
*/
int PhysicsBody::getId() const { return id; }

// gets the object this physics body is attachted to
Object* PhysicsBody::getObject() const { return object; }

// sets the object
void PhysicsBody::setObject(Object* obj) { object = obj; }

// attaches to an object
PhysicsBody* PhysicsBody::attachToObject(Object * newObj)
{
	object = newObj;
	// newObj->addPhysicsBody(this); // adds the physics body if it isn't there already
	return this;
}

// gets the model position
glm::vec3 PhysicsBody::getModelPosition() const { return position; }

// sets the model position
void PhysicsBody::setModelPosition(glm::vec3 mpos) { position = mpos; }

// returns the world position; objects save their position in world space
glm::vec3 PhysicsBody::getWorldPosition() const 
{
	// TODO : change this to take a Vec3
	return (object == nullptr ? position : glm::vec3(object->getPosition() + position));
}

// sets the world position
void PhysicsBody::setWorldPosition(glm::vec3 wpos) { position = wpos; }

// calculations collision between objects
bool PhysicsBody::collision(PhysicsBody* p1, PhysicsBody* p2)
{
	// if either object is null.
	if (p1 == nullptr || p2 == nullptr)
		return false;

	// no physics body type attachted
	if (p1->getId() == 0 || p2->getId() == 0)
		return false;

	// AABB Collision
	if (p1->getId() == 1 && p2->getId() == 1)
	{
		PhysicsBodyBox * temp1 = (PhysicsBodyBox*)p1;
		PhysicsBodyBox * temp2 = (PhysicsBodyBox*)p2;

		// origin is the centre of the 
		// minimum values of A
		util::math::Vec3 minA (
			temp1->getWorldPosition().x - temp1->getWidth() / 2.0F, 
			temp1->getWorldPosition().y - temp1->getHeight() / 2.0F, 
			temp1->getWorldPosition().z - temp1->getDepth() / 2.0F
		);

		// maximum values of A
		util::math::Vec3 maxA(
			temp1->getWorldPosition().x + temp1->getWidth() / 2.0F,
			temp1->getWorldPosition().y + temp1->getHeight() / 2.0F,
			temp1->getWorldPosition().z + temp1->getDepth() / 2.0F
		);

		// minimum values of B
		util::math::Vec3 minB(
			temp2->getWorldPosition().x - temp2->getWidth() / 2.0F,
			temp2->getWorldPosition().y - temp2->getHeight() / 2.0F,
			temp2->getWorldPosition().z - temp2->getDepth() / 2.0F
		);

		// maximum values of B
		util::math::Vec3 maxB(
			temp2->getWorldPosition().x + temp2->getWidth() / 2.0F,
			temp2->getWorldPosition().y + temp2->getHeight() / 2.0F,
			temp2->getWorldPosition().z + temp2->getDepth() / 2.0F
		);

		return util::math::aabbCollision(minA, maxA, minB, maxB);
	}
	else if (p1->getId() == 2 && p2->getId() == 2) // sphere-sphere
	{
		PhysicsBodySphere* temp1 = (PhysicsBodySphere*)p1;
		PhysicsBodySphere* temp2 = (PhysicsBodySphere*)p2;

		// gets the world positions
		glm::vec3 tv1 = temp1->getWorldPosition();
		glm::vec3 tv2 = temp2->getWorldPosition();

		return util::math::sphereCollision(util::math::Vec3(tv1.x, tv1.y, tv1.z), temp1->getRadius(), util::math::Vec3(tv2.x, tv2.y, tv2.z), temp2->getRadius());
	}

	return false;
}

// PHYSICS BODY BOX
// constructor
PhysicsBodyBox::PhysicsBodyBox(float width, float height, float depth) : 
	PhysicsBodyBox(glm::vec3(), width, height, depth)
{}

// sets position
PhysicsBodyBox::PhysicsBodyBox(float x, float y, float z, float width, float height, float depth) 
	: PhysicsBodyBox(glm::vec3(x, y, z), width, height, depth)
{}

// location of the physics body (relative to object origin), and dimensions
PhysicsBodyBox::PhysicsBodyBox(glm::vec3 position, float width, float height, float depth)
	: PhysicsBody(1, position), width(abs(width)), height(abs(height)), depth(abs(depth))// , box(width, height, depth)
{
	// box.setColor(255, 0, 0); // RED
}

// location of the physics body and its dimensions
PhysicsBodyBox::PhysicsBodyBox(glm::vec3 position, glm::vec3 dimensions)
	: PhysicsBodyBox(position, dimensions.x, dimensions.y, dimensions.z)
{}

// gets the width
float PhysicsBodyBox::getWidth() const { return width; }

// sets the width
void PhysicsBodyBox::setWidth(float newWidth) { width = newWidth; }

// returns the height
float PhysicsBodyBox::getHeight() const { return height; }

// sets the height
void PhysicsBodyBox::setHeight(float newHeight) { height = newHeight; }

// returns depth
float PhysicsBodyBox::getDepth() const { return depth; }

// sets the depth
void PhysicsBodyBox::setDepth(float newDepth) { depth = newDepth; }

// toString
std::string PhysicsBodyBox::toString() const
{
	std::string posStr = "(" + std::to_string(position.x) + ", " + std::to_string(position.y) + ", " + std::to_string(position.z) + ")";

	return "Model Position: " + posStr +
		" | Width: " + std::to_string(width) +
		" | Height: " + std::to_string(height) +
		" | Depth: " + std::to_string(depth);
}

PhysicsBodySphere::PhysicsBodySphere(float radius) : PhysicsBody(2), radius(radius) {}

PhysicsBodySphere::PhysicsBodySphere(glm::vec3 pos, float radius) : PhysicsBody(2, pos), radius(radius) {}

// radius of the sphere
float PhysicsBodySphere::getRadius() const { return radius; }

// sets the radius
void PhysicsBodySphere::setRadius(float r) { radius = abs(r); }

// to string function
std::string PhysicsBodySphere::toString() const
{
	return std::string("Model Position: " + std::to_string(position.x) + std::to_string(position.y) + std::to_string(position.z)
					 + " | Radius: " + std::to_string(radius));
}
