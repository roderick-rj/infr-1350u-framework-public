#pragma once
#include "Primitive.h"

typedef class PrimitiveCylinder : public Primitive
{
public:
	// creates a cylinder
	// segments: the amount of segments for the depth of the cylinder
	PrimitiveCylinder(float radius = 1.0F, float height = 1.0F, unsigned int segments = 10);
private:
protected:
	
} Cylinder;

