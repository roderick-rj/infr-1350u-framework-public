#include "Game.h"
#include <stdexcept>
#define IMGUI_IMPL_OPENGL_LOADER_GLAD
#include <imgui.h>
#include <imgui_impl_opengl3.h>
#include <imgui_impl_glfw.h>
#include <GLM/gtc/matrix_transform.hpp>

#include <functional>

#include "SceneManager.h"
#include "MeshRenderer.h"

#include "utils/math/Collision.h" // collision
#include "utils/math/Rotation.h" // rotation
// #include <string>

struct TempTransform {
	glm::vec3 Position = glm::vec3(0.0f);
	glm::vec3 EulerRotation = glm::vec3(0.0f);
	glm::vec3 Scale = glm::vec3(1.0f);

	// does our TRS for us.
	glm::mat4 GetWorldTransform() const {
		return
			glm::translate(glm::mat4(1.0f), Position) *
			glm::mat4_cast(glm::quat(glm::radians(EulerRotation))) *
			glm::scale(glm::mat4(1.0f), Scale)
			;
	}
};

struct UpdateBehaviour {
	std::function<void(entt::entity e, float dt)> Function;
};

// call this function to  resize the window.
void GlfwWindowResizedCallback(GLFWwindow* window, int width, int height) {
	glViewport(0, 0, width, height);
	Game* game = (Game*)glfwGetWindowUserPointer(window);

	if (game != nullptr)
	{
		game->Resize(width, height);
	}
}

// called when a mouse button event is recorded
void MouseButtomCallback(GLFWwindow* window, int button, int action, int mods) {
	Game* game = (Game*)glfwGetWindowUserPointer(window);

	// returns the function early if this isn't a game class
	if (game == nullptr) {
		return;
	}

	switch (action) {
	case GLFW_PRESS:
		game->MouseButtonPressed(window, button);
		break;
	case GLFW_RELEASE:
		game->MouseButtonReleased(window, button);
		break;
	}
}

// called when a key has been pressed, held down, or released. This function figures out which, and calls the appropriate function to handle it.
// KeyCallback(Window, Keyboard Key, Platform-Specific Scancode, Key Action, and Modifier Bits)
void KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	Game* game = (Game*)glfwGetWindowUserPointer(window);

	if (game != nullptr)
	{
		// checks for what type of button press happened.
		switch (action)
		{
		case GLFW_PRESS: // key has been pressed
			game->KeyPressed(window, key);
			break;

		case GLFW_REPEAT: // key is held down
			game->KeyHeld(window, key);
			break;

		case GLFW_RELEASE: // key has been released
			game->KeyReleased(window, key);
			break;
		}
	}
}



Game::Game() :
	myWindow(nullptr),
	myWindowTitle("ICG_1 - Exam - 100701758"),
	myClearColor(glm::vec4(0.1f, 0.7f, 0.5f, 1.0f))
{ }

Game::~Game() { }

void Game::Initialize() {
	// Initialize GLFW
	if (glfwInit() == GLFW_FALSE) {
		std::cout << "Failed to initialize GLFW" << std::endl;
		throw std::runtime_error("Failed to initialize GLFW");
	}
	// Enable transparent backbuffers for our windows (note that Windows expects our colors to be pre-multiplied with alpha)
	glfwWindowHint(GLFW_TRANSPARENT_FRAMEBUFFER, true);
	// Create a new GLFW window
	myWindow = glfwCreateWindow(600, 600, myWindowTitle, nullptr, nullptr);
	// We want GL commands to be executed for our window, so we make our window's context the current one
	glfwMakeContextCurrent(myWindow);
	// Let glad know what function loader we are using (will call gl commands via glfw)
	if (gladLoadGLLoader((GLADloadproc)glfwGetProcAddress) == 0) {
		std::cout << "Failed to initialize Glad" << std::endl;
		throw std::runtime_error("Failed to initialize GLAD");
	}

	// setting up window user pointer so that we can resize our window
	// Tie our game to our window, so we can access it via callbacks
	glfwSetWindowUserPointer(myWindow, this);
	glEnable(GL_DEPTH_TEST);
	// Set our window resized callback
	glfwSetWindowSizeCallback(myWindow, GlfwWindowResizedCallback);
	
	// Setting keyboard callback function
	glfwSetKeyCallback(myWindow, KeyCallback);

	// Setting mouse button callback function
	glfwSetMouseButtonCallback(myWindow, MouseButtomCallback);

}

void Game::Shutdown() {
	glfwTerminate();
}

// loads the content for the meshes and shaders
void Game::LoadContent() 
{
	myCamera = std::make_shared<Camera>();
	myCamera->SetPosition(glm::vec3(5, 5, 20));
	myCamera->LookAt(glm::vec3(0.0f, 0.0f, 0.0f));

	// sets the camera to perspective mode.
	cameraAngle = glm::radians(60.0f);
	myCamera->Projection = glm::perspective(cameraAngle, 1.0f, 0.01f, 1000.0f);
	perspectiveCamera = true; // in perspective mode.

	// MESH 1 (PLANE)
	// Create our 4 vertices
	// we're using an initalizer list inside an initializer list to get the data
	Vertex bgVerts[4] = {
		// Position				  Color							Normal
		//  x      y	 z		   r	 g	   b	 a		   x  y  z
		{{ -10.0f, -10.0f, -10.0f }, { 1.0f, 0.0f, 0.0f, 1.0f }, {0, 0, 1}},
		{{ 10.0f, -10.0f, -10.0f },  { 1.0f, 1.0f, 0.0f, 1.0f }, {0, 0, 1}},
		{{ -10.0f, 10.0f, -10.0f },  { 1.0f, 0.0f, 1.0f, 1.0f }, {0, 0, 1}},
		{{ 10.0f, 10.0f, -10.0f },   { 0.0f, 1.0f, 0.0f, 1.0f }, {0, 0, 1}},
	};

	// Create our 6 indices
	uint32_t bgInds[6] = {
	0, 1, 2,
	2, 1, 3
	};
	
	// Create a new mesh from the data
	myMesh = std::make_shared<Mesh>(bgVerts, 4, bgInds, 6);
	
	// MESH (2, 3) PLAYERS
	pWidth = 2.0F;
	pHeight = 1.0F;
	pDepth = 1.0F;

	const unsigned int plyrVertsTotal = 8; // total vertices
	const unsigned int plyrIndsTotal = 36; // total indices

	p1Pos = glm::vec3(0.0F, 8.0F, 0.0F);
	p2Pos = glm::vec3(0.0F, -8.0F, 0.0F);

	glm::vec3 norm1 = { 0.0F, 0.0F, 1.0F };
	glm::vec3 norm2 = { 0.0F, 0.0F, 1.0F };
	glm::vec3 norm3 = { 0.0F, 0.0F, 1.0F };
	glm::vec3 norm4 = { 0.0F, 0.0F, 1.0F };


	glm::vec3 norm5 = { 0.0F, 0.0F, -1.0F };
	glm::vec3 norm6 = { 0.0F, 0.0F, -1.0F };
	glm::vec3 norm7 = { 0.0F, 0.0F, -1.0F };
	glm::vec3 norm8 = { 0.0F, 0.0F, -1.0F };

	util::math::Vec3 normT(0.0F, 0.0F, 1.0F); // uses my personal vector class for transformations.
	util::math::Vec3 temp; // gets individual normal transform
	
	normT = util::math::rotateZ(normT, 45.0F, true); // rotates normT.
	// sets all the normals; if this is wrong then I probably ended up not using them.
	// the normals are wrong but I'll use them anyway
	// Top
	temp = util::math::rotateX(normT, 0.0F, true);
	norm1 = glm::vec3(temp.x, temp.y, temp.z);

	temp = util::math::rotateX(normT, 90.0F, true);
	norm2 = glm::vec3(temp.x, temp.y, temp.z);

	temp = util::math::rotateX(normT, 180.0F, true);
	norm3 = glm::vec3(temp.x, temp.y, temp.z);

	temp = util::math::rotateX(normT, 270.0F, true);
	norm4 = glm::vec3(temp.x, temp.y, temp.z);

	// Bottom
	normT = util::math::rotateZ(util::math::Vec3(0.0F, 0.0F, -1.0F), 45.0F, true); // rotates normT.

	temp = util::math::rotateX(normT, 0.0F, true);
	norm5 = glm::vec3(temp.x, temp.y, temp.z);

	temp = util::math::rotateX(normT, 90.0F, true);
	norm6 = glm::vec3(temp.x, temp.y, temp.z);

	temp = util::math::rotateX(normT, 180.0F, true);
	norm7 = glm::vec3(temp.x, temp.y, temp.z);

	temp = util::math::rotateX(normT, 270.0F, true);
	norm8 = glm::vec3(temp.x, temp.y, temp.z);

	// MESH 2 (PADDLE)
	//Vertex plyrVerts[4] = {
	//	// Position				  Color							Normal
	//	//  x      y	 z										r	 g	   b	 a		   x  y  z
	//	{{ -plyrSize[0] / 2.0F, -plyrSize[1] / 2.0F, 1.0f }, { 1.0f, 0.0f, 0.0f, 1.0f }, {0, 0, 1}},
	//	{{ plyrSize[0] / 2.0F, -plyrSize[1] / 2.0F, 1.0f },  { 1.0f, 1.0f, 0.0f, 1.0f }, {0, 0, 1}},
	//	{{ -plyrSize[0] / 2.0F, plyrSize[1] / 2.0F, 1.0f },  { 1.0f, 0.0f, 1.0f, 1.0f }, {0, 0, 1}},
	//	{{ plyrSize[0] / 2.0F, plyrSize[1] / 2.0F, 1.0f },   { 0.0f, 1.0f, 0.0f, 1.0f }, {0, 0, 1}},
	//};

	Vertex plyrVerts[plyrVertsTotal] = {
		// Position				  Color							Normal
		//  x      y	 z		   r	 g	   b	 a		   x  y  z
		{{ -pWidth / 2.0F, -pHeight / 2.0F,  pDepth / 2.0F}, { 1.0f, 1.0f, 1.0f, 1.0f }, norm1}, // bottom left, front corner
		{{ -pWidth / 2.0F,  pHeight / 2.0F,  pDepth / 2.0F}, { 1.0f, 1.0f, 1.0f, 1.0f }, norm2}, // top left, front corner
		{{ -pWidth / 2.0F, -pHeight / 2.0F, -pDepth / 2.0F}, { 1.0f, 1.0f, 1.0f, 1.0f }, norm3}, // bottom left, back corner
		{{ -pWidth / 2.0F,  pHeight / 2.0F, -pDepth / 2.0F}, { 1.0f, 1.0f, 1.0f, 1.0f }, norm4}, // top left, back corner

		{{ pWidth / 2.0F, -pHeight / 2.0F,  pDepth / 2.0F}, { 1.0f, 1.0f, 1.0f, 1.0f }, norm5}, // bottom right, front corner
		{{ pWidth / 2.0F,  pHeight / 2.0F,  pDepth / 2.0F}, { 1.0f, 1.0f, 1.0f, 1.0f }, norm6}, // top right, front corner
		{{ pWidth / 2.0F, -pHeight / 2.0F, -pDepth / 2.0F}, { 1.0f, 1.0f, 1.0f, 1.0f }, norm7}, // bottom right, back corner
		{{ pWidth / 2.0F,  pHeight / 2.0F, -pDepth / 2.0F}, { 1.0f, 1.0f, 1.0f, 1.0f }, norm8} // top right, back corner
	};
	
	Vertex p1Verts[plyrVertsTotal]; // vertices for p1
	Vertex p2Verts[plyrVertsTotal]; // vertices for p2

	// Create our 6 indices
	//uint32_t plyrInds[6] = {
	//0, 1, 2,
	//2, 1, 3
	//};


	// Indices for player
	uint32_t plyrInds[plyrIndsTotal] = {
		0, 1, 2, // front face - top half
		2, 1, 3, // front face - bottom half
		0, 4, 2, // right side-face (box's perspective) - top half
		2, 4, 6, // right side-face (box's perspectice) - bottom half
		1, 5, 3, // left side-face - top half
		3, 5, 7, // left side-face, bottom half
		4, 5, 6, // back face - top half
		6, 5, 7, // back face - bottom half
		0, 4, 5, // top tri 1
		5, 0, 1, //  top tri 2
		2, 6, 7, // bottom tri 1
		7, 2, 3 // bottom tri 2
	};

	// sets the positions of each player
	for (int i = 0; i < plyrVertsTotal; i++)
	{
		// setting the location for p1 and p2
		p1Verts[i] = plyrVerts[i];
		p1Verts[i].Position += p1Pos;

		p2Verts[i] = plyrVerts[i];
		p2Verts[i].Position += p2Pos;
	}


	// Create a new mesh from the data
	p1Mesh = std::make_shared<Mesh>(p1Verts, plyrVertsTotal, plyrInds, plyrIndsTotal);
	p2Mesh = std::make_shared<Mesh>(p2Verts, plyrVertsTotal, plyrInds, plyrIndsTotal);

	// MESH 3 (PUCK)
	puckRadius = 0.5F;
	puckMesh = PrimitiveSphere(puckRadius).getMesh(); // re-using the sphere clas since that's the only object that works for some reason.


	// objects (that don't work, aside for sphere for some reason)
	objects.push_back(new PrimitiveCube(2.0F, 2.0F, 2.0F));
	objects.push_back(new PrimitivePlane(2.0F, 2.0f));
	objects.push_back(new PrimitiveSphere(2.0F));

	// SHADER
	Shader::Sptr phong = std::make_shared<Shader>();
	phong->Load("lighting.vs.glsl", "blinn-phong.fs.glsl");

	

	Material::Sptr testMat = std::make_shared<Material>(phong);
	testMat->Set("a_LightPos", { 0, 0, 0.5 });
	testMat->Set("a_LightColor", { 1.0f, 1.0f, 0 });
	testMat->Set("a_AmbientColor", { 1.0f, 1.0f, 1.0f });
	testMat->Set("a_AmbientPower", 0.8f); // change this to change the main lighting power (originally value of 0.1F)
	testMat->Set("a_LightSpecPower", 0.8f);
	testMat->Set("a_LightShininess", 256);
	testMat->Set("a_LightAttenuation", 1.0f);

	SceneManager::RegisterScene("Scene 1");
	SceneManager::RegisterScene("Scene 2");
	SceneManager::SetCurrentScene("Scene 1");

	{
		// adds an entity to one of the scenes
		auto& ecs = GetRegistry("Scene 1");
		{
			entt::entity e1 = ecs.create();
			MeshRenderer& m1 = ecs.assign<MeshRenderer>(e1);
			m1.Material = testMat;
			m1.Mesh = myMesh;


			//auto rotate = [](entt::entity e, float dt) {
			//	auto& transform = CurrentRegistry().get_or_assign<TempTransform>(e);
			//	transform.EulerRotation += glm::vec3(0, 0, 90 * dt);

			//	// does the same thing, except all in one linel.
			//	// CurrentRegistry().get_or_assign<TempTransform>(e).EulerRotation += glm::vec3(0, 0, 90 * dt);
			//};
			//auto& up = ecs.get_or_assign<UpdateBehaviour>(e1);
			//up.Function = rotate;
		}

		// player 1 object
		{
			entt::entity e2 = ecs.create();
			MeshRenderer& m2 = ecs.assign<MeshRenderer>(e2);
			m2.Material = testMat;
			m2.Mesh = p1Mesh;

			// (&) captures everything outside of the scope.
			auto rotate = [&](entt::entity e, float dt) {
				glm::vec3 translate(0.0F, 0.0F, 0.0F); // translation vector

				// movement increment
				if (p1Ctrls[0]) // move up
				{
					translate.x= pMoveInc.x * dt;
				}
				else if (p1Ctrls[1]) // move down
				{
					translate.x = -pMoveInc.x * dt;
				}

				auto& transform = CurrentRegistry().get_or_assign<TempTransform>(e);
				// transform.EulerRotation += glm::vec3(0, 0, 90 * dt);
				transform.Position += translate; // translates the mesh
				p1Pos = transform.Position; // saving position

				// does the same thing, except all in one linel.
				// CurrentRegistry().get_or_assign<TempTransform>(e).EulerRotation += glm::vec3(0, 0, 90 * dt);
			};
			auto& up = ecs.get_or_assign<UpdateBehaviour>(e2);
			up.Function = rotate;
		}

		// player 2 object
		{
			entt::entity e3 = ecs.create();
			MeshRenderer& m3 = ecs.assign<MeshRenderer>(e3);
			m3.Material = testMat;
			m3.Mesh = p2Mesh;

			// (&) captures everything outside of the scope.
			auto rotate = [&](entt::entity e, float dt) {
				glm::vec3 translate(0.0F, 0.0F, 0.0F); // translation vector

				// movement increment
				if (p2Ctrls[0]) // move up
				{
					translate.x = pMoveInc.x * dt;
				}
				else if (p2Ctrls[1]) // move down
				{
					translate.x = -pMoveInc.x * dt;
				}

				auto& transform = CurrentRegistry().get_or_assign<TempTransform>(e);
				// transform.EulerRotation += glm::vec3(0, 0, 90 * dt);
				transform.Position += translate; // translates the mesh
				p2Pos = transform.Position; // saving position

				// does the same thing, except all in one linel.
				// CurrentRegistry().get_or_assign<TempTransform>(e).EulerRotation += glm::vec3(0, 0, 90 * dt);
			};
			auto& up = ecs.get_or_assign<UpdateBehaviour>(e3);
			up.Function = rotate;
		}

		// puck object
		{
			entt::entity e4 = ecs.create();
			MeshRenderer& m4 = ecs.assign<MeshRenderer>(e4);
			m4.Material = testMat;
			m4.Mesh = puckMesh;

			// (&) captures everything outside of the scope.
			auto rotate = [&](entt::entity e, float dt) {
				
				glm::vec3 translate(0.0F, 0.0F, 0.0F); // translation vector
				
				translate += puckMoveInc * dt; // translates the ball

				auto& transform = CurrentRegistry().get_or_assign<TempTransform>(e);
				// transform.EulerRotation += glm::vec3(0, 0, 90 * dt);

				// resetting the position if the puck goes too far.
				if (transform.Position.x > 20.0F || transform.Position.x < -20.0F)
					transform.Position.x = 0.0F;

				if (transform.Position.y > 20.0F || transform.Position.y < -20.0F)
					transform.Position.y = 0.0F;

				if (transform.Position.z > 20.0F || transform.Position.z < -20.0F)
					transform.Position.z = 0.0F;

				transform.Position += translate; // translates the mesh

				puckPos = transform.Position; // saving position
				// does the same thing, except all in one linel.
				// CurrentRegistry().get_or_assign<TempTransform>(e).EulerRotation += glm::vec3(0, 0, 90 * dt);
			};
			auto& up = ecs.get_or_assign<UpdateBehaviour>(e4);
			up.Function = rotate;
		}
	}


	/// EX ///
	// second set of vertices
	Vertex vertices2[4] = 
	{
		// Position Color
		{{ -0.5f + 0.5f, -0.5f + 0.5f, 0.0f }, { 1.0f / 2.0f, 0.0f, 0.0f, 1.0f }, {0.0F, 0.0F, 1.0F}},
		{{ 0.5f + 0.5f, -0.5f + 0.5f, 0.0f }, { 1.0f / 2.0f, 1.0f, 0.0f, 1.0f }, {0.0F, 0.0F, 1.0F}},
		{{ -0.5f + 0.5f, 0.5f + 0.5f, 0.0f }, { 1.0f / 2.0f, 0.0f, 1.0f / 1.5f, 1.0f }, {0.0F, 0.0F, 1.0F}},
		{{ 0.5f + 0.5f, 0.5f + 0.5f, 0.0f }, { 0.0f, 1.0f / 1.5f, 0.0f, 1.0f }, {0.0F, 0.0F, 1.0F}},
	};
	
	// second set of indices
	// Create our 6 indices
	uint32_t indices2[6] = 
	{
	0, 1, 2,
	2, 1, 3
	};
	
	myMesh2 = std::make_shared<Mesh>(vertices2, 4, bgInds, 6);

	//{
	//	// adds an entity to one of the scenes
	//	auto& ecs = GetRegistry("Test");
	//	entt::entity e1 = ecs.create();
	//	MeshRenderer& m1 = ecs.assign<MeshRenderer>(e1);
	//	m1.Material = testMat;
	//	m1.Mesh = myMesh2;

	//	auto rotate = [](entt::entity e, float dt) {
	//		auto& transform = CurrentRegistry().get_or_assign<TempTransform>(e);
	//		transform.EulerRotation += glm::vec3(0, 0, 90 * dt);

	//		// does the same thing, except all in one linel.
	//		// CurrentRegistry().get_or_assign<TempTransform>(e).EulerRotation += glm::vec3(0, 0, 90 * dt);
	//	};
	//	auto& up = ecs.get_or_assign<UpdateBehaviour>(e1);
	//	up.Function = rotate;
	//}

	// addign in pre-existing objects
	


	// No longer needed since we have dedicated .glsl files.
	// all the new lines and whatnot will go into one string using R'LIT
	/*const char* vs_source = R"LIT(
		#version 410
		layout (location = 0) in vec3 inPosition;
		layout (location = 1) in vec4 inColor;

		layout (location = 0) out vec4 outColor;
		void main() {
		outColor = inColor;
		gl_Position = vec4(inPosition, 1);
		}
		)LIT";

	// output colour is just what we passed in, we aren't modifying it for now.
	const char* fs_source = R"LIT(
		#version 410
		layout (location = 0) in vec4 inColor;
		layout (location = 0) out vec4 outColor;
		void main() {
		outColor = inColor;
		}
		)LIT";*/

	// Create and compile shader
	myShader = std::make_shared<Shader>();
	// myShader->Compile(vs_source, fs_source); // no longer needed since we have a dedicated file.
	myShader->Load("passthrough.vert.glsl", "passthrough.frag.glsl");
	
	myModelTransform = glm::mat4(1.0f);
}

void Game::UnloadContent() {
}

void Game::Update(float deltaTime) {

	// Original Code

	//glm::vec3 movement = glm::vec3(0.0f);
	//glm::vec3 rotation = glm::vec3(0.0f);
	//static glm::vec3 objMovement = glm::vec3(0.0f);

	//float speed = 2.5f; // changed from 1.0F
	//float rotSpeed = 2.5f;

	//// set to 'true' to have the object rotate. This prevents you from moving forward and backward.
	//bool objRotate = true;

	//if (glfwGetKey(myWindow, GLFW_KEY_W) == GLFW_PRESS)
	//	movement.z -= speed * deltaTime;
	//if (glfwGetKey(myWindow, GLFW_KEY_S) == GLFW_PRESS)
	//	movement.z += speed * deltaTime;
	//if (glfwGetKey(myWindow, GLFW_KEY_A) == GLFW_PRESS)
	//	movement.x -= speed * deltaTime;
	//if (glfwGetKey(myWindow, GLFW_KEY_D) == GLFW_PRESS)
	//	movement.x += speed * deltaTime;
	//if (glfwGetKey(myWindow, GLFW_KEY_SPACE) == GLFW_PRESS)
	//	movement.y += speed * deltaTime;
	//if (glfwGetKey(myWindow, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS)
	//	movement.y -= speed * deltaTime;

	//if (glfwGetKey(myWindow, GLFW_KEY_Q) == GLFW_PRESS)
	//	rotation.z -= rotSpeed * deltaTime;
	//if (glfwGetKey(myWindow, GLFW_KEY_E) == GLFW_PRESS)
	//	rotation.z += rotSpeed * deltaTime;
	//if (glfwGetKey(myWindow, GLFW_KEY_UP) == GLFW_PRESS)
	//	rotation.x -= rotSpeed * deltaTime;
	//if (glfwGetKey(myWindow, GLFW_KEY_DOWN) == GLFW_PRESS)
	//	rotation.x += rotSpeed * deltaTime;
	//if (glfwGetKey(myWindow, GLFW_KEY_LEFT) == GLFW_PRESS)
	//	rotation.y -= rotSpeed * deltaTime;
	//if (glfwGetKey(myWindow, GLFW_KEY_RIGHT) == GLFW_PRESS)
	//	rotation.y += rotSpeed * deltaTime;

	// if 'c' is pressed, the camera mode is changed.
	
	static bool isKeyDown = false; // makes it so that the key isn't constantly detected if it is released.
	if (glfwGetKey(myWindow, GLFW_KEY_C) == GLFW_PRESS)
	{
		if (!isKeyDown) {
			perspectiveCamera = !perspectiveCamera;

			// perpsective (fovRadians, aspect, zNear, zFar
			// ortho (left, right, bottom, top, near, far)

			// switches camera mode
			myCamera->Projection = (perspectiveCamera) ?
				glm::perspective(cameraAngle, 1.0f, 0.01f, 1000.0f) : glm::ortho(-5.0f, 5.0f, -5.0f, 5.0f, 0.0f, 100.0f);
		}
		isKeyDown = true;
	}
	else {
		isKeyDown = false;
	}

	//if(!objRotate)
	//	myModelTransform = glm::mat4(1.0f); // resets the transform matrix so that it's an identity matrix.
	//
	//										// translation of the object.
	//if (glfwGetKey(myWindow, GLFW_KEY_I) == GLFW_PRESS) // move up (y-axis up)
	//{
	//	if (objRotate)
	//	{
	//		myModelTransform = glm::translate(myModelTransform, glm::vec3(0.0f, 0.0f, speed * deltaTime));
	//	}
	//	else
	//	{
	//		objMovement += glm::vec3(0.0f, 0.0f, speed * deltaTime);
	//	}	
	//}
	//else if (glfwGetKey(myWindow, GLFW_KEY_K) == GLFW_PRESS) // move down (y-axis down)
	//{
	//	if (objRotate)
	//	{
	//		myModelTransform = glm::translate(myModelTransform, glm::vec3(0.0f, 0.0f, -speed * deltaTime));
	//	}
	//	else
	//	{
	//		objMovement += glm::vec3(0.0f, 0.0f, -speed * deltaTime);
	//	}
	//}
	//else if (glfwGetKey(myWindow, GLFW_KEY_J) == GLFW_PRESS) // move left (x-axis left)
	//{
	//	if (objRotate)
	//	{
	//		myModelTransform = glm::translate(myModelTransform, glm::vec3(-speed * deltaTime, 0.0f, 0.0f));
	//	}
	//	else
	//	{
	//		objMovement += glm::vec3(-speed * deltaTime, 0.0f, 0.0f);
	//	}
	//}
	//else if (glfwGetKey(myWindow, GLFW_KEY_L) == GLFW_PRESS) // move right (x-axis right)
	//{
	//	if (objRotate)
	//	{
	//		myModelTransform = glm::translate(myModelTransform, glm::vec3(speed * deltaTime, 0.0f, 0.0f));
	//	}
	//	else
	//	{
	//		objMovement += glm::vec3(speed * deltaTime, 0.0f, 0.0f);
	//	}
	//}

	//if(!objRotate)
	//	myModelTransform = glm::translate(myModelTransform, objMovement);
	//
	//// Rotate and move our camera based on input
	//myCamera->Rotate(rotation);
	//myCamera->Move(movement);

	//// Rotate our transformation matrix a little bit each frame
	//// Doesn't work if myModelTransform is set to an identity matrix
	//myModelTransform = glm::rotate(myModelTransform, deltaTime, glm::vec3(0, 0, 1));

	//// calling all of our functions for our update behaviours.
	auto view = CurrentRegistry().view<UpdateBehaviour>();
	for (const auto& e : view) {
		auto& func = CurrentRegistry().get<UpdateBehaviour>(e);
		if (func.Function) {
			func.Function(e, deltaTime);
		}
	}

	Collision(); // checks for collision.
}

void Game::InitImGui() {
	// Creates a new ImGUI context
	ImGui::CreateContext();
	// Gets our ImGUI input/output
	ImGuiIO& io = ImGui::GetIO();
	// Enable keyboard navigation
	io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
	// Allow docking to our window
	io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;
	// Allow multiple viewports (so we can drag ImGui off our window)
	io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;
	// Allow our viewports to use transparent backbuffers
	io.ConfigFlags |= ImGuiConfigFlags_TransparentBackbuffers;
	// Set up the ImGui implementation for OpenGL
	ImGui_ImplGlfw_InitForOpenGL(myWindow, true);
	ImGui_ImplOpenGL3_Init("#version 410");

	// Dark mode FTW
	ImGui::StyleColorsDark();
	// Get our imgui style
	ImGuiStyle& style = ImGui::GetStyle();
	//style.Alpha = 1.0f;
	if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable) {
		style.WindowRounding = 0.0f;
		style.Colors[ImGuiCol_WindowBg].w = 0.8f;
	}
}

void Game::ShutdownImGui() {
	// Cleanup the ImGui implementation
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	// Destroy our ImGui context
	ImGui::DestroyContext();
}

void Game::ImGuiNewFrame() {
	// Implementation new frame
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplGlfw_NewFrame();
	// ImGui context new frame
	ImGui::NewFrame();
}

void Game::ImGuiEndFrame() {
	// Make sure ImGui knows how big our window is
	ImGuiIO& io = ImGui::GetIO();
	int width{ 0 }, height{ 0 };
	glfwGetWindowSize(myWindow, &width, &height);
	io.DisplaySize = ImVec2(width, height);
	// Render all of our ImGui elements
	ImGui::Render();
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
	// If we have multiple viewports enabled (can drag into a new window)
	if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable) {
		// Update the windows that ImGui is using
		ImGui::UpdatePlatformWindows();
		ImGui::RenderPlatformWindowsDefault();
		// Restore our gl context
		glfwMakeContextCurrent(myWindow);
	}
}

void Game::Run()
{
	Initialize();
	InitImGui();
	LoadContent();
	static float prevFrame = glfwGetTime();
	// Run as long as the window is open
	while (!glfwWindowShouldClose(myWindow)) {
		// Poll for events from windows
		// clicks, key presses, closing, all that
		glfwPollEvents();
		float thisFrame = glfwGetTime();
		float deltaTime = thisFrame - prevFrame;
		Update(deltaTime);
		Draw(deltaTime);
		ImGuiNewFrame();
		DrawGui(deltaTime);
		ImGuiEndFrame();
		prevFrame = thisFrame;
		// Present our image to windows
		glfwSwapBuffers(myWindow);

		// added so that the previous frame is updated. Otherwise, it would just be since the beginning of the program.
		prevFrame = thisFrame;
	}
	UnloadContent();
	ShutdownImGui();
	Shutdown();
}

// resizes the window and keeps size proportionate.
void Game::Resize(int newWidth, int newHeight)
{
	// set to float since we're calculating the new projecction as the screen size.
	if(perspectiveCamera) // camera is in perspective mode
	{
		myCamera->Projection = glm::perspective(cameraAngle, newWidth / (float)newHeight, 0.01f, 1000.0f);
	}
	else // camera is in orthographic mode
	{
		myCamera->Projection = glm::ortho(-5.0f * newWidth / (float)newHeight, 5.0f * newWidth / (float)newHeight, -5.0f, 5.0f, 0.0f, 100.0f);
	}
	
}

// called when a mouse button has been pressed
void Game::MouseButtonPressed(GLFWwindow* window, int button) {
	Game* game = (Game*)glfwGetWindowUserPointer(window);

	if (game == nullptr) // if game is 'null', then nothing happens
		return;

	// checks each button
	switch (button) {
	case GLFW_MOUSE_BUTTON_LEFT:
		break;
	case GLFW_MOUSE_BUTTON_MIDDLE:
		break;
	case GLFW_MOUSE_BUTTON_RIGHT:
		break;
	}
}

// called when a mouse button has been pressed
void Game::MouseButtonReleased(GLFWwindow* window, int button) {
	Game* game = (Game*)glfwGetWindowUserPointer(window);

	if (game == nullptr) // if game is 'null', then nothing happens
		return;

	// checks each button
	switch (button) {
	case GLFW_MOUSE_BUTTON_LEFT:
		break;
	case GLFW_MOUSE_BUTTON_MIDDLE:
		break;
	case GLFW_MOUSE_BUTTON_RIGHT:
		break;
	}
}

// key has been pressed
void Game::KeyPressed(GLFWwindow* window, int key)
{
	Game* game = (Game*)glfwGetWindowUserPointer(window);

	if (game == nullptr) // if game is 'null', then nothing happens
		return;

	// checks key value.
	switch (key)
	{
		// PLAYER 1
	case GLFW_KEY_W: // up
		p1Ctrls[0] = true;
		break;
	case GLFW_KEY_S: // down
		p1Ctrls[1] = true;
		break;
	case GLFW_KEY_A: // left
		p1Ctrls[2] = true;
		break;
	case GLFW_KEY_D: // right
		p1Ctrls[3] = true;
		break;

		// PLAYER 2
	case GLFW_KEY_UP: // up
		p2Ctrls[0] = true;
		break;
	case GLFW_KEY_DOWN: // down
		p2Ctrls[1] = true;
		break;
	case GLFW_KEY_LEFT: // left
		p2Ctrls[2] = true;
		break;
	case GLFW_KEY_RIGHT: // right
		p2Ctrls[3] = true;
		break;
	}
}

// key is being held
void Game::KeyHeld(GLFWwindow* window, int key)
{
	Game* game = (Game*)glfwGetWindowUserPointer(window);

	if (game == nullptr) // if game is 'null', then it is returned
		return;

	switch (key)
	{
		// PLAYER 1
	case GLFW_KEY_W: // up
		p1Ctrls[0] = true;
		break;
	case GLFW_KEY_S: // down
		p1Ctrls[1] = true;
		break;
	case GLFW_KEY_A: // left
		p1Ctrls[2] = true;
		break;
	case GLFW_KEY_D: // right
		p1Ctrls[3] = true;
		break;

		// PLAYER 2
	case GLFW_KEY_UP: // up
		p2Ctrls[0] = true;
		break;
	case GLFW_KEY_DOWN: // down
		p2Ctrls[1] = true;
		break;
	case GLFW_KEY_LEFT: // left
		p2Ctrls[2] = true;
		break;
	case GLFW_KEY_RIGHT: // right
		p2Ctrls[3] = true;
		break;
	}
}

// key hs been released
void Game::KeyReleased(GLFWwindow* window, int key)
{
	Game* game = (Game*)glfwGetWindowUserPointer(window);

	if (game == nullptr) // if game is 'null', then it is returned
		return;

	switch (key)
	{
		// PLAYER 1
	case GLFW_KEY_W: // up
		p1Ctrls[0] = false;
		break;
	case GLFW_KEY_S: // down
		p1Ctrls[1] = false;
		break;
	case GLFW_KEY_A: // left
		p1Ctrls[2] = false;
		break;
	case GLFW_KEY_D: // right
		p1Ctrls[3] = false;
		break;

		// PLAYER 2
	case GLFW_KEY_UP: // up
		p2Ctrls[0] = false;
		break;
	case GLFW_KEY_DOWN: // down
		p2Ctrls[1] = false;
		break;
	case GLFW_KEY_LEFT: // left
		p2Ctrls[2] = false;
		break;
	case GLFW_KEY_RIGHT: // right
		p2Ctrls[3] = false;
		break;
	}
}


void Game::Draw(float deltaTime) {
	// Clear our screen every frame
	glClearColor(myClearColor.x, myClearColor.y, myClearColor.z, myClearColor.w);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	// We'll grab a reference to the ecs to make things easier
	auto& ecs = CurrentRegistry();
	// We sort our mesh renderers based on material properties
	// This will group all of our meshes based on shader first, then material second
	ecs.sort<MeshRenderer>([](const MeshRenderer& lhs, const MeshRenderer& rhs) {
		if (rhs.Material == nullptr || rhs.Mesh == nullptr)
			return false;
		else if (lhs.Material == nullptr || lhs.Mesh == nullptr)
			return true;
		else if (lhs.Material->GetShader() != rhs.Material->GetShader())
			return lhs.Material->GetShader() < rhs.Material->GetShader();
		else
			return lhs.Material < rhs.Material;
		});

	// These will keep track of the current shader and material that we have bound
	Material::Sptr mat = nullptr;
	Shader::Sptr boundShader = nullptr;
	// A view will let us iterate over all of our entities that have the given component types
	auto view = ecs.view<MeshRenderer>();

	for (const auto& entity : view) {
		// Get our shader
		const MeshRenderer& renderer = ecs.get<MeshRenderer>(entity);
		// Early bail if mesh is invalid
		if (renderer.Mesh == nullptr || renderer.Material == nullptr)
			continue;
		// If our shader has changed, we need to bind it and update our frame-level uniforms
		if (renderer.Material->GetShader() != boundShader) {
			boundShader = renderer.Material->GetShader();
			boundShader->Bind();
			boundShader->SetUniform("a_CameraPos", myCamera->GetPosition());
		}
		// If our material has changed, we need to apply it to the shader
		if (renderer.Material != mat) {
			mat = renderer.Material;
			mat->Apply();
		}

		// We'll need some info about the entities position in the world
		const TempTransform& transform = ecs.get_or_assign<TempTransform>(entity);
		// Get the object's transformation
		glm::mat4 worldTransform = transform.GetWorldTransform();
		// Our normal matrix is the inverse-transpose of our object's world rotation
		// Recall that everything's backwards in GLM
		glm::mat3 normalMatrix = glm::mat3(glm::transpose(glm::inverse(worldTransform)));

		// Update the MVP using the item's transform
		mat->GetShader()->SetUniform(
			"a_ModelViewProjection",
			myCamera->GetViewProjection() *
			worldTransform);
		// Update the model matrix to the item's world transform
		mat->GetShader()->SetUniform("a_Model", worldTransform);
		// Update the model matrix to the item's world transform
		mat->GetShader()->SetUniform("a_NormalMatrix", normalMatrix);
		// Draw the item
		renderer.Mesh->Draw();

	}

	//// Original
	//// Clear our screen every frame
	//glClearColor(myClearColor.x, myClearColor.y, myClearColor.z, myClearColor.w);
	//glClear(GL_COLOR_BUFFER_BIT);

	//myShader->Bind();

	//// myShader->SetUniform("a_ModelViewProjection", myCamera->GetViewProjection());
	//myShader->SetUniform("a_ModelViewProjection", myCamera->GetViewProjection() * myModelTransform); // allows for slow rotation

	// myMesh->Draw();
	// Update our uniform

	// myMesh2->Draw();

	// draws each object
	//for (Object* obj : objects)
	//{
	//	if(obj->getVisible())
	//		obj->getMesh()->Draw();
	//}
}

void Game::DrawGui(float deltaTime) {
	// Open a new ImGui window
	ImGui::Begin("Colour Picker");
	
	// Draw widgets here
	// ImGui::SliderFloat4("Color", &myClearColor.x, 0, 1); // Original
	ImGui::ColorPicker4("Color", &myClearColor.x); // new version
	if (ImGui::InputText("Title", myWindowTitle, 31))
	{
		glfwSetWindowTitle(myWindow, myWindowTitle);
	}

	if (ImGui::Button("Apply")) // adding another button, which allows for the application of the window title.
	{
		glfwSetWindowTitle(myWindow, myWindowTitle);
	}

	ImGui::Text("C: Change Camera Mode");
	// ImGui::Text(("Time: " + std::to_string(glfwGetTime())).c_str()); // requires inclusion of <string>

	// draws a button for each scene name.
	auto it = SceneManager::Each();
	for (auto& kvp : it) {
		if (ImGui::Button(kvp.first.c_str())) {
			SceneManager::SetCurrentScene(kvp.first);
		}
	}

	ImGui::End();

	// Creating a Second Window
	//ImGui::Begin("Test 2");
	//ImGui::Text("Hello Cruel World!");
	//ImGui::End();
}

// collision calculations
bool Game::Collision()
{
	// Based on a quick look, all objects start at position (0, 0, 0), even if their vertices don't put their origins at (0, 0, 0).
	// So collision is registering even though the paddles aren't in the right places, because internally they're still at (0, 0, 0).
	// To fix this I would have to spawn them at the origin, and then move them, but as you can see I didn't have time for that.
	// Regardless, the collision algorithm still works.
	bool col = false;

	// std::cout << "P1_POS: (" << p1Pos.x << ", " << p1Pos.y << ", " << p1Pos.z << ");" << std::endl;
	// std::cout << "P2_POS: (" << p2Pos.x << ", " << p2Pos.y << ", " << p2Pos.z << ");" << std::endl;
	// std::cout << "PUCK_POS: (" << puckPos.x << ", " << puckPos.y << ", " << puckPos.z << ");" << std::endl;
	// std::cout << std::endl;

	// player 1 collision with the puck
	col = util::math::aabbCollision(
		util::math::Vec3(p1Pos.x - pWidth, p1Pos.y - pHeight, p1Pos.z - pDepth),
		util::math::Vec3(p1Pos.x + pWidth, p1Pos.y + pHeight, p1Pos.z + pDepth),
		util::math::Vec3(puckPos.x - puckRadius, puckPos.y - puckRadius, puckPos.z - puckRadius),
		util::math::Vec3(puckPos.x + puckRadius, puckPos.y + puckRadius, puckPos.z + puckRadius)
	);

	// collision is equal to 'true'
	if (col)
	{
		puckMoveInc.y *= -1;
		return col;
	}

	// player 2 collision with the puck
	col = util::math::aabbCollision(
		util::math::Vec3(p2Pos.x - pWidth, p2Pos.y - pHeight, p2Pos.z - pDepth),
		util::math::Vec3(p2Pos.x + pWidth, p2Pos.y + pHeight, p2Pos.z + pDepth),
		util::math::Vec3(puckPos.x - puckRadius, puckPos.y - puckRadius, puckPos.z - puckRadius),
		util::math::Vec3(puckPos.x + puckRadius, puckPos.y + puckRadius, puckPos.z + puckRadius)
	);

	// collision is equal to 'true'
	if (col)
	{
		puckMoveInc.y *= -1;
		return col;
	}

	return false;
}
