/*
 * Name: Roderick "R.J." Montague
 * Student Number: 100701758
 * Date: 10/28/2019
 * Description: air hockey game in OpenGL.
*/

// INTRODUCTION TO COMPUTER GRAPHICS (INFR 1350U) - PRACTICAL EXAM
// 1) AIR HOCKEY
#include "Game.h"
#include <Logging.h>

#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

int main()
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF); // checks for memory leaks once the program ends.
	long long allocPoint = 0;
	if (allocPoint)
	{
		_CrtSetBreakAlloc(allocPoint); // sets where you want to stop our program by assigning the allocation block index stopping point.
	}

	Logger::Init();

	Game* game = new Game();
	game->Run();
	delete game;

	Logger::Uninitialize();

	return 0;
}