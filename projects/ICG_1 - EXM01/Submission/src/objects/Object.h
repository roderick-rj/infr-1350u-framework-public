// OBJECT CLASS (HEADER)
#pragma once

#include <string>
#include <fstream> // uses the fstream file reading method.
#include <vector>
#include <math.h>

#include "..\Shader.h"
#include "..\Mesh.h"
#include "..\Camera.h"

class PhysicsBody; // forward declaring

// object loader
class Object
{
public:
	// the name and directory of the .obj file
	Object(std::string filePath);

	// destructor
	~Object();

	// returns the file name and file path
	std::string getFile() const;

	// gets the name of the object.
	std::string getName() const;

	// sets the name of the object.
	void setName(std::string newName);

	// gets the desc of the object.
	std::string getDescription() const;

	// sets the desc of the object.
	void setDescription(std::string newDesc);

	// returns 'true' if the file is safe to use, and 'false' if it isn't. If it's false, then something is wrong with the file.
	bool getSafe();

	// returns the color as a glm::vec3
	// this returns only the colour of the first vertex, so if other vertices have different colours, they are not set.
	glm::vec4 getColor() const;

	// sets the colour based on a range of 0 to 255 for the RGB values. Alpha (a) stll ranges from 0.0 to 1.0
	// this overrides ALL RGB values for all vertices
	void setColor(int r, int g, int b, float a = 1.0F);

	// sets the colour based on a range of 0.0 to 1.0 (OpenGL default)
	// this overrides ALL RGBA values for all vertices
	void setColor(float r, float g, float b, float a = 1.0F);

	// sets the colour of the mesh. This leaves out the alpha (a) value, which is set to whatever it is for the first vertex.
	void setColor(glm::vec3 color);

	// sets the colour of the mesh (RGBA [0-1]).
	void setColor(glm::vec4 color);

	// returns a reference to the mesh.
	Mesh::Sptr& getMesh();

	// returns whether the mesh is being drawn
	bool getVisible();

	// set whether the object is visible or not.
	void setVisible(bool draw);

	// gets the position
	glm::vec3 getPosition() const;

	// sets the position
	void setPosition(float x, float y, float z);

	// sets the position
	void setPosition(glm::vec3 newPos);

	// adds a physics body; returns true if added. The same physics body can't be added twice.
	bool addPhysicsBody(PhysicsBody* body);

	//// removes a physics body; returns 'true' if successful.
	bool removePhysicsBody(PhysicsBody* body);

	//// removes a physics body based on its index.
	bool removePhysicsBody(unsigned int index);

	// gets the amount of physics bodies
	unsigned int getPhysicsBodyCount() const;

	// gets the physics bodies
	std::vector<PhysicsBody*> getPhysicsBodies() const;

	// gets whether the object intersects with another object.
	bool getIntersection() const;

	// sets whether the object is currently intersecting with another object.
	void setIntersection(bool inter);

	// updates the object
	void update();

	// the maximum amount of vertices one object can have. This doesn't get used.
	const static unsigned int VERTICES_MAX;

	// the maximum amount of indices one object can have. This doesn't get used.
	const static unsigned int INDICES_MAX;

private:
	// called to load the object
	bool loadObject();

	// parses the line, gets the values as data type T, and stores them in a vector.
	// containsSymbol: tells the function if the string passed still contains the symbol at the start. If so, it is removed before the parsing begins.
	// *** the symbol at the start is what's used to determine what the values in a given line of a .obj are for.
	template<typename T>
	const std::vector<T> parseStringForTemplate(std::string str, bool containsSymbol = true);

	// the string for the file path
	std::string filePath = "";

	// object name
	std::string name = "";

	// object description
	std::string description = "";

	// true if the file is safe to read from, false otherwise.
	bool safe = false;

	// a vector of physics bodies
	std::vector<PhysicsBody*> bodies;

	// becomes 'true' when an object intersects something.
	bool intersection = false;

	// draws the mesh when 'visible' is true, hides it if false.
	bool visible = true;

protected:
	// constructor used for default primitives
	Object();

	// the position of the object.
	glm::vec3 position;

	// a dynamic array of vertices for the 3D model.
	Vertex* vertices = nullptr;

	// the number of vertices that exist for the 3D model.
	unsigned int verticesTotal = 0;

	// a dynamic array of indices for the 3D model.
	uint32_t* indices = nullptr;

	// the total number of indices 
	unsigned int indicesTotal = 0;

	// texture UV array - this doesn't do anything for this assignment
	glm::vec2* vertexTextures = nullptr;

	// total amount of texture vertices
	unsigned int vertexTexturesTotal = 0;

	// the array for vertex normals - doesn't do anythin for this project
	glm::vec3* vertexNormals = nullptr;

	// total amount of vertex normals
	unsigned int vertexNormalsTotal = 0;

	// the mesh
	Mesh::Sptr mesh;
};

