#pragma once
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "GLM/glm.hpp"

#include "Shader.h"
#include "Mesh.h"
#include "Camera.h"

#include "objects/PrimitivePlane.h"
#include "objects/PrimitiveCube.h"
#include "objects/PrimitiveSphere.h"

#include <vector>
#include <iostream>

class Game {
public:
	Game();
	
	~Game();
	
	void Run();

	void Resize(int newWidth, int newHeight);

	// called when a mouse button has been pressed
	void MouseButtonPressed(GLFWwindow* window, int button);

	// called when a mouse button has been pressed
	void MouseButtonReleased(GLFWwindow* window, int button);

	// called when a key has been pressed
	virtual void KeyPressed(GLFWwindow* window, int key);

	// called when a key is being held down
	virtual void KeyHeld(GLFWwindow* window, int key);

	// called when a key has been released
	virtual void KeyReleased(GLFWwindow* window, int key);

protected:
	void Initialize();
	
	void Shutdown();
	
	void LoadContent();
	
	void UnloadContent();
	
	void InitImGui();
	
	void ShutdownImGui();
	
	void ImGuiNewFrame();
	
	void ImGuiEndFrame();
	
	void Update(float deltaTime);
	
	void Draw(float deltaTime);
	
	void DrawGui(float deltaTime);

private:
	// called to calculate collision
	bool Collision();

	// Stores the main window that the game is running in
	GLFWwindow* myWindow;

	// Stores the clear color of the game's window
	glm::vec4 myClearColor;
	
	// Stores the title of the game's window
	char myWindowTitle[32];

	// A shared pointer to our mesh
	Mesh::Sptr myMesh;
	
	// player 1 mesh
	Mesh::Sptr p1Mesh;

	// Puck Mesh
	Mesh::Sptr puckMesh;

	// player 2 mesh
	Mesh::Sptr p2Mesh;

	// A shared pointer to our shader
	Shader::Sptr myShader;

	// Extra Shader for exercise.
	Mesh::Sptr myMesh2;
	// Shader::Sptr myShader2; // not needed

	// a vector of objects
	std::vector<Object *> objects;

	Camera::Sptr myCamera;
	bool perspectiveCamera = false; // if 'true', the perpsective camera is used. If 'false', orthographic camera is used.
	float cameraAngle = 60.0F;

	// Our models transformation matrix
	glm::mat4 myModelTransform;

	// CONTROL RELATED
	// [0] = UP, [1] = DOWN, [2] = LEFT, [3] = RIGHT
	bool p1Ctrls[4] = { false, false, false, false };
	
	bool p2Ctrls[4] = { false, false, false, false };

	// moves the player
	glm::vec3 pMoveInc = { 10.0f, 0.0F, 0.0F };

	glm::vec3 p1Pos = { 0.0f, 0.0F, 0.0F };; // player 1's position
	glm::vec3 p2Pos = { 0.0f, 0.0F, 0.0F };; // player 2's position

	float pWidth; // width of players
	float pHeight; // height of players
	float pDepth; // depth of players

	// the radius of the puck. This will just be used for square collision
	glm::vec3 puckPos = { 0.0f, 0.0F, 0.0F }; // puck's position
	float puckRadius;
	glm::vec3 puckMoveInc = { -2.0f, -2.0f, 0.0f }; // movement for the puck

	// bool p1Up = false, p1Down = false, p1Left = false, p1Right = false;
	// bool p2Up = false, p2Down = false, p2Left = false, p2Right = false;
};