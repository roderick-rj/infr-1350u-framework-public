INFR 1350U: Introduction to Computer Graphics
Assignment 2/Assignment 3 - Viewport Viewer/Terrain Renderer

Name: Roderick "R.J." Montague
Student Number: 100701758

CONTROLS
********
The controls are listed below.
- Left Mouse Button (Click): Select Viewport (active viewport surrounded with red border)
- Right Mouse Button (Hold): Rotate Camera (Perspective Mode only)
- W/Up Arrow: up camera up
- S/Down Arrow: move camera down
- A/Left Arrow: move camera left
- D/Right Arrow: move camera right


REFERENCES
***********************
The object loader and files related to its functionality are adapted from my team's GDW code. They are still rather similar to the framework versions, but have noticeable changes.
I don't think anyone in my team used an adaptation of said code, but if they did, their names are listed below. We are from team 'Bonus Fruit':
- Jonah Griffin (100702748)  
- Kennedy Adams (100632983)
- Nathan Tuck (100708651)  
- Ryan Burton (100707511)    
- Spencer Tester (100653129)
- Stephane "Steph" Gagnon (100694227) 

The extra code in Utils.h and Utils.cpp was taken from my own personal project, which I've used for other assignments before, including those in this program.
As usual if these register a plagiarism flag, that is why. I have the original project in full available for evidence in the event that foul play is suspected.


The skybox, height map, and default texture files were provided by the course.
Shown below are the sources for the texture files used in the engine.
	- sand.png: https://opengameart.org/content/seamless-beach-sand
	- grass_02.png: https://opengameart.org/content/grass-1
	- rock.png: https://opengameart.org/sites/default/files/Rock1Seamless_1.png
	- lava.png: https://opengameart.org/content/molten-metal-texture
		- This was meant to be for the icosphere, but the loader wasn't working, and I don't think it's worth fixing since this is optional.
		- As such, while this is included, it isn't used.
