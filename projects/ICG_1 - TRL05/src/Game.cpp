#include "Game.h"
#include <stdexcept>
#define IMGUI_IMPL_OPENGL_LOADER_GLAD
#include <imgui.h>
#include <imgui_impl_opengl3.h>
#include <imgui_impl_glfw.h>
#include <GLM/gtc/matrix_transform.hpp>

#include <functional>

#include "SceneManager.h"
#include "MeshRenderer.h"
// #include <string>

struct TempTransform {
	glm::vec3 Position = glm::vec3(0.0f);
	glm::vec3 EulerRotation = glm::vec3(0.0f);
	glm::vec3 Scale = glm::vec3(1.0f);

	// does our TRS for us.
	glm::mat4 GetWorldTransform() const {
		return
			glm::translate(glm::mat4(1.0f), Position) *
			glm::mat4_cast(glm::quat(glm::radians(EulerRotation))) *
			glm::scale(glm::mat4(1.0f), Scale)
			;
	}
};

struct UpdateBehaviour {
	std::function<void(entt::entity e, float dt)> Function;
};

// call this function to  resize the window.
void GlfwWindowResizedCallback(GLFWwindow* window, int width, int height) {
	glViewport(0, 0, width, height);
	Game* game = (Game*)glfwGetWindowUserPointer(window);

	if (game != nullptr)
	{
		game->Resize(width, height);
	}
}

Game::Game() :
	myWindow(nullptr),
	myWindowTitle("ICG_1 - TRL05"),
	myClearColor(glm::vec4(0.1f, 0.7f, 0.5f, 1.0f))
{ }

Game::~Game() { }

void Game::Initialize() {
	// Initialize GLFW
	if (glfwInit() == GLFW_FALSE) {
		std::cout << "Failed to initialize GLFW" << std::endl;
		throw std::runtime_error("Failed to initialize GLFW");
	}
	// Enable transparent backbuffers for our windows (note that Windows expects our colors to be pre-multiplied with alpha)
	glfwWindowHint(GLFW_TRANSPARENT_FRAMEBUFFER, true);
	// Create a new GLFW window
	myWindow = glfwCreateWindow(600, 600, myWindowTitle, nullptr, nullptr);
	// We want GL commands to be executed for our window, so we make our window's context the current one
	glfwMakeContextCurrent(myWindow);
	// Let glad know what function loader we are using (will call gl commands via glfw)
	if (gladLoadGLLoader((GLADloadproc)glfwGetProcAddress) == 0) {
		std::cout << "Failed to initialize Glad" << std::endl;
		throw std::runtime_error("Failed to initialize GLAD");
	}

	// setting up window user pointer so that we can resize our window
	// Tie our game to our window, so we can access it via callbacks
	glfwSetWindowUserPointer(myWindow, this);
	// Set our window resized callback
	glfwSetWindowSizeCallback(myWindow, GlfwWindowResizedCallback);

}

void Game::Shutdown() {
	glfwTerminate();
}

// loads the content for the meshes and shaders
void Game::LoadContent() 
{
	myCamera = std::make_shared<Camera>();
	myCamera->SetPosition(glm::vec3(5, 5, 5));
	myCamera->LookAt(glm::vec3(0));

	// sets the camera to perspective mode.
	myCamera->Projection = glm::perspective(glm::radians(60.0f), 1.0f, 0.01f, 1000.0f);
	perspectiveCamera = true; // in perspective mode.


	// Create our 4 vertices
	// we're using an initalizer list inside an initializer list to get the data
	Vertex vertices[4] = {
		// Position				  Color							Normal
		//  x      y	 z		   r	 g	   b	 a		   x  y  z
		{{ -2.5f, -2.5f, 0.0f }, { 1.0f, 0.0f, 0.0f, 1.0f }, {0, 0, 1}},
		{{ 2.5f, -2.5f, 0.0f },  { 1.0f, 1.0f, 0.0f, 1.0f }, {0, 0, 1}},
		{{ -2.5f, 2.5f, 0.0f },  { 1.0f, 0.0f, 1.0f, 1.0f }, {0, 0, 1}},
		{{ 2.5f, 2.5f, 0.0f },   { 0.0f, 1.0f, 0.0f, 1.0f }, {0, 0, 1}},
	};
	// Create our 6 indices
	uint32_t indices[6] = {
	0, 1, 2,
	2, 1, 3
	};

	Shader::Sptr phong = std::make_shared<Shader>();
	phong->Load("lighting.vs.glsl", "blinn-phong.fs.glsl");

	// Create a new mesh from the data
	myMesh = std::make_shared<Mesh>(vertices, 4, indices, 6);

	Material::Sptr testMat = std::make_shared<Material>(phong);
	testMat->Set("a_LightPos", { 0, 0, 1 });
	testMat->Set("a_LightColor", { 1.0f, 1.0f, 0 });
	testMat->Set("a_AmbientColor", { 1.0f, 1.0f, 1.0f });
	testMat->Set("a_AmbientPower", 0.1f); // change this to change the main lighting power (originally value of 0.1F)
	testMat->Set("a_LightSpecPower", 0.5f);
	testMat->Set("a_LightShininess", 256);
	testMat->Set("a_LightAttenuation", 1.0f);

	SceneManager::RegisterScene("Test");
	SceneManager::RegisterScene("Test2");
	SceneManager::SetCurrentScene("Test");

	{
		// adds an entity to one of the scenes
		auto& ecs = GetRegistry("Test");
		entt::entity e1 = ecs.create();
		MeshRenderer& m1 = ecs.assign<MeshRenderer>(e1);
		m1.Material = testMat;
		m1.Mesh = myMesh;

		auto rotate = [](entt::entity e, float dt) {
			auto& transform = CurrentRegistry().get_or_assign<TempTransform>(e);
			transform.EulerRotation += glm::vec3(0, 0, 90 * dt);

			// does the same thing, except all in one linel.
			// CurrentRegistry().get_or_assign<TempTransform>(e).EulerRotation += glm::vec3(0, 0, 90 * dt);
		};
		auto& up = ecs.get_or_assign<UpdateBehaviour>(e1);
		up.Function = rotate;
	}


	/// EX ///
	// second set of vertices
	Vertex vertices2[4] = 
	{
		// Position Color
		{{ -0.5f + 0.5f, -0.5f + 0.5f, 0.0f }, { 1.0f / 2.0f, 0.0f, 0.0f, 1.0f }},
		{{ 0.5f + 0.5f, -0.5f + 0.5f, 0.0f }, { 1.0f / 2.0f, 1.0f, 0.0f, 1.0f }},
		{{ -0.5f + 0.5f, 0.5f + 0.5f, 0.0f }, { 1.0f / 2.0f, 0.0f, 1.0f / 1.5f, 1.0f }},
		{{ 0.5f + 0.5f, 0.5f + 0.5f, 0.0f }, { 0.0f, 1.0f / 1.5f, 0.0f, 1.0f }},
	};
	
	// second set of indices
	// Create our 6 indices
	uint32_t indices2[6] = 
	{
	0, 1, 2,
	2, 1, 3
	};
	
	myMesh2 = std::make_shared<Mesh>(vertices2, 4, indices, 6);

	// No longer needed since we have dedicated .glsl files.
	// all the new lines and whatnot will go into one string using R'LIT
	/*const char* vs_source = R"LIT(
		#version 410
		layout (location = 0) in vec3 inPosition;
		layout (location = 1) in vec4 inColor;

		layout (location = 0) out vec4 outColor;
		void main() {
		outColor = inColor;
		gl_Position = vec4(inPosition, 1);
		}
		)LIT";

	// output colour is just what we passed in, we aren't modifying it for now.
	const char* fs_source = R"LIT(
		#version 410
		layout (location = 0) in vec4 inColor;
		layout (location = 0) out vec4 outColor;
		void main() {
		outColor = inColor;
		}
		)LIT";*/

	// Create and compile shader
	myShader = std::make_shared<Shader>();
	// myShader->Compile(vs_source, fs_source); // no longer needed since we have a dedicated file.
	myShader->Load("passthrough.vert.glsl", "passthrough.frag.glsl");
	
	myModelTransform = glm::mat4(1.0f);
}

void Game::UnloadContent() {
}

void Game::Update(float deltaTime) {
	glm::vec3 movement = glm::vec3(0.0f);
	glm::vec3 rotation = glm::vec3(0.0f);
	static glm::vec3 objMovement = glm::vec3(0.0f);

	float speed = 2.5f; // changed from 1.0F
	float rotSpeed = 2.5f;

	// set to 'true' to have the object rotate. This prevents you from moving forward and backward.
	bool objRotate = true;

	if (glfwGetKey(myWindow, GLFW_KEY_W) == GLFW_PRESS)
		movement.z -= speed * deltaTime;
	if (glfwGetKey(myWindow, GLFW_KEY_S) == GLFW_PRESS)
		movement.z += speed * deltaTime;
	if (glfwGetKey(myWindow, GLFW_KEY_A) == GLFW_PRESS)
		movement.x -= speed * deltaTime;
	if (glfwGetKey(myWindow, GLFW_KEY_D) == GLFW_PRESS)
		movement.x += speed * deltaTime;
	if (glfwGetKey(myWindow, GLFW_KEY_SPACE) == GLFW_PRESS)
		movement.y += speed * deltaTime;
	if (glfwGetKey(myWindow, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS)
		movement.y -= speed * deltaTime;

	if (glfwGetKey(myWindow, GLFW_KEY_Q) == GLFW_PRESS)
		rotation.z -= rotSpeed * deltaTime;
	if (glfwGetKey(myWindow, GLFW_KEY_E) == GLFW_PRESS)
		rotation.z += rotSpeed * deltaTime;
	if (glfwGetKey(myWindow, GLFW_KEY_UP) == GLFW_PRESS)
		rotation.x -= rotSpeed * deltaTime;
	if (glfwGetKey(myWindow, GLFW_KEY_DOWN) == GLFW_PRESS)
		rotation.x += rotSpeed * deltaTime;
	if (glfwGetKey(myWindow, GLFW_KEY_LEFT) == GLFW_PRESS)
		rotation.y -= rotSpeed * deltaTime;
	if (glfwGetKey(myWindow, GLFW_KEY_RIGHT) == GLFW_PRESS)
		rotation.y += rotSpeed * deltaTime;

	// if 'c' is pressed, the camera mode is changed.
	
	static bool isKeyDown = false; // makes it so that the key isn't constantly detected if it is released.
	if (glfwGetKey(myWindow, GLFW_KEY_C) == GLFW_PRESS)
	{
		if (!isKeyDown) {
			perspectiveCamera = !perspectiveCamera;

			// perpsective (fovRadians, aspect, zNear, zFar
			// ortho (left, right, bottom, top, near, far)

			// switches camera mode
			myCamera->Projection = (perspectiveCamera) ?
				glm::perspective(glm::radians(60.0f), 1.0f, 0.01f, 1000.0f) : glm::ortho(-5.0f, 5.0f, -5.0f, 5.0f, 0.0f, 100.0f);
		}
		isKeyDown = true;
	}
	else {
		isKeyDown = false;
	}

	if(!objRotate)
		myModelTransform = glm::mat4(1.0f); // resets the transform matrix so that it's an identity matrix.
	
											// translation of the object.
	if (glfwGetKey(myWindow, GLFW_KEY_I) == GLFW_PRESS) // move up (y-axis up)
	{
		if (objRotate)
		{
			myModelTransform = glm::translate(myModelTransform, glm::vec3(0.0f, 0.0f, speed * deltaTime));
		}
		else
		{
			objMovement += glm::vec3(0.0f, 0.0f, speed * deltaTime);
		}	
	}
	else if (glfwGetKey(myWindow, GLFW_KEY_K) == GLFW_PRESS) // move down (y-axis down)
	{
		if (objRotate)
		{
			myModelTransform = glm::translate(myModelTransform, glm::vec3(0.0f, 0.0f, -speed * deltaTime));
		}
		else
		{
			objMovement += glm::vec3(0.0f, 0.0f, -speed * deltaTime);
		}
	}
	else if (glfwGetKey(myWindow, GLFW_KEY_J) == GLFW_PRESS) // move left (x-axis left)
	{
		if (objRotate)
		{
			myModelTransform = glm::translate(myModelTransform, glm::vec3(-speed * deltaTime, 0.0f, 0.0f));
		}
		else
		{
			objMovement += glm::vec3(-speed * deltaTime, 0.0f, 0.0f);
		}
	}
	else if (glfwGetKey(myWindow, GLFW_KEY_L) == GLFW_PRESS) // move right (x-axis right)
	{
		if (objRotate)
		{
			myModelTransform = glm::translate(myModelTransform, glm::vec3(speed * deltaTime, 0.0f, 0.0f));
		}
		else
		{
			objMovement += glm::vec3(speed * deltaTime, 0.0f, 0.0f);
		}
	}

	if(!objRotate)
		myModelTransform = glm::translate(myModelTransform, objMovement);
	
	// Rotate and move our camera based on input
	myCamera->Rotate(rotation);
	myCamera->Move(movement);

	// Rotate our transformation matrix a little bit each frame
	// Doesn't work if myModelTransform is set to an identity matrix
	myModelTransform = glm::rotate(myModelTransform, deltaTime, glm::vec3(0, 0, 1));

	// calling all of our functions for our update behaviours.
	auto view = CurrentRegistry().view<UpdateBehaviour>();
	for (const auto& e : view) {
		auto& func = CurrentRegistry().get<UpdateBehaviour>(e);
		if (func.Function) {
			func.Function(e, deltaTime);
		}
	}
}

void Game::InitImGui() {
	// Creates a new ImGUI context
	ImGui::CreateContext();
	// Gets our ImGUI input/output
	ImGuiIO& io = ImGui::GetIO();
	// Enable keyboard navigation
	io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
	// Allow docking to our window
	io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;
	// Allow multiple viewports (so we can drag ImGui off our window)
	io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;
	// Allow our viewports to use transparent backbuffers
	io.ConfigFlags |= ImGuiConfigFlags_TransparentBackbuffers;
	// Set up the ImGui implementation for OpenGL
	ImGui_ImplGlfw_InitForOpenGL(myWindow, true);
	ImGui_ImplOpenGL3_Init("#version 410");

	// Dark mode FTW
	ImGui::StyleColorsDark();
	// Get our imgui style
	ImGuiStyle& style = ImGui::GetStyle();
	//style.Alpha = 1.0f;
	if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable) {
		style.WindowRounding = 0.0f;
		style.Colors[ImGuiCol_WindowBg].w = 0.8f;
	}
}

void Game::ShutdownImGui() {
	// Cleanup the ImGui implementation
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	// Destroy our ImGui context
	ImGui::DestroyContext();
}

void Game::ImGuiNewFrame() {
	// Implementation new frame
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplGlfw_NewFrame();
	// ImGui context new frame
	ImGui::NewFrame();
}

void Game::ImGuiEndFrame() {
	// Make sure ImGui knows how big our window is
	ImGuiIO& io = ImGui::GetIO();
	int width{ 0 }, height{ 0 };
	glfwGetWindowSize(myWindow, &width, &height);
	io.DisplaySize = ImVec2(width, height);
	// Render all of our ImGui elements
	ImGui::Render();
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
	// If we have multiple viewports enabled (can drag into a new window)
	if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable) {
		// Update the windows that ImGui is using
		ImGui::UpdatePlatformWindows();
		ImGui::RenderPlatformWindowsDefault();
		// Restore our gl context
		glfwMakeContextCurrent(myWindow);
	}
}

void Game::Run()
{
	Initialize();
	InitImGui();
	LoadContent();
	static float prevFrame = glfwGetTime();
	// Run as long as the window is open
	while (!glfwWindowShouldClose(myWindow)) {
		// Poll for events from windows
		// clicks, key presses, closing, all that
		glfwPollEvents();
		float thisFrame = glfwGetTime();
		float deltaTime = thisFrame - prevFrame;
		Update(deltaTime);
		Draw(deltaTime);
		ImGuiNewFrame();
		DrawGui(deltaTime);
		ImGuiEndFrame();
		prevFrame = thisFrame;
		// Present our image to windows
		glfwSwapBuffers(myWindow);

		// added so that the previous frame is updated. Otherwise, it would just be since the beginning of the program.
		prevFrame = thisFrame;
	}
	UnloadContent();
	ShutdownImGui();
	Shutdown();
}

// resizes the window and keeps size proportionate.
void Game::Resize(int newWidth, int newHeight)
{
	// set to float since we're calculating the new projecction as the screen size.
	if(perspectiveCamera) // camera is in perspective mode
	{
		myCamera->Projection = glm::perspective(glm::radians(60.0f), newWidth / (float)newHeight, 0.01f, 1000.0f);
	}
	else // camera is in orthographic mode
	{
		myCamera->Projection = glm::ortho(-5.0f * newWidth / (float)newHeight, 5.0f * newWidth / (float)newHeight, -5.0f, 5.0f, 0.0f, 100.0f);
	}
	
}

void Game::Draw(float deltaTime) {
	// Clear our screen every frame
	glClearColor(myClearColor.x, myClearColor.y, myClearColor.z, myClearColor.w);
	glClear(GL_COLOR_BUFFER_BIT);

	// We'll grab a reference to the ecs to make things easier
	auto& ecs = CurrentRegistry();
	// We sort our mesh renderers based on material properties
	// This will group all of our meshes based on shader first, then material second
	ecs.sort<MeshRenderer>([](const MeshRenderer& lhs, const MeshRenderer& rhs) {
		if (rhs.Material == nullptr || rhs.Mesh == nullptr)
			return false;
		else if (lhs.Material == nullptr || lhs.Mesh == nullptr)
			return true;
		else if (lhs.Material->GetShader() != rhs.Material->GetShader())
			return lhs.Material->GetShader() < rhs.Material->GetShader();
		else
			return lhs.Material < rhs.Material;
		});

	// These will keep track of the current shader and material that we have bound
	Material::Sptr mat = nullptr;
	Shader::Sptr boundShader = nullptr;
	// A view will let us iterate over all of our entities that have the given component types
	auto view = ecs.view<MeshRenderer>();

	for (const auto& entity : view) {
		// Get our shader
		const MeshRenderer& renderer = ecs.get<MeshRenderer>(entity);
		// Early bail if mesh is invalid
		if (renderer.Mesh == nullptr || renderer.Material == nullptr)
			continue;
		// If our shader has changed, we need to bind it and update our frame-level uniforms
		if (renderer.Material->GetShader() != boundShader) {
			boundShader = renderer.Material->GetShader();
			boundShader->Bind();
			boundShader->SetUniform("a_CameraPos", myCamera->GetPosition());
		}
		// If our material has changed, we need to apply it to the shader
		if (renderer.Material != mat) {
			mat = renderer.Material;
			mat->Apply();
		}

		// We'll need some info about the entities position in the world
		const TempTransform& transform = ecs.get_or_assign<TempTransform>(entity);
		// Get the object's transformation
		glm::mat4 worldTransform = transform.GetWorldTransform();
		// Our normal matrix is the inverse-transpose of our object's world rotation
		// Recall that everything's backwards in GLM
		glm::mat3 normalMatrix = glm::mat3(glm::transpose(glm::inverse(worldTransform)));

		// Update the MVP using the item's transform
		mat->GetShader()->SetUniform(
			"a_ModelViewProjection",
			myCamera->GetViewProjection() *
			worldTransform);
		// Update the model matrix to the item's world transform
		mat->GetShader()->SetUniform("a_Model", worldTransform);
		// Update the model matrix to the item's world transform
		mat->GetShader()->SetUniform("a_NormalMatrix", normalMatrix);
		// Draw the item
		renderer.Mesh->Draw();
	}
}

void Game::DrawGui(float deltaTime) {
	// Open a new ImGui window
	ImGui::Begin("Colour Picker");
	
	// Draw widgets here
	// ImGui::SliderFloat4("Color", &myClearColor.x, 0, 1); // Original
	ImGui::ColorPicker4("Color", &myClearColor.x); // new version
	if (ImGui::InputText("Title", myWindowTitle, 31))
	{
		glfwSetWindowTitle(myWindow, myWindowTitle);
	}

	if (ImGui::Button("Apply")) // adding another button, which allows for the application of the window title.
	{
		glfwSetWindowTitle(myWindow, myWindowTitle);
	}

	ImGui::Text("C: Change Camera Mode");
	// ImGui::Text(("Time: " + std::to_string(glfwGetTime())).c_str()); // requires inclusion of <string>

	// draws a button for each scene name.
	auto it = SceneManager::Each();
	for (auto& kvp : it) {
		if (ImGui::Button(kvp.first.c_str())) {
			SceneManager::SetCurrentScene(kvp.first);
		}
	}

	ImGui::End();

	// Creating a Second Window
	//ImGui::Begin("Test 2");
	//ImGui::Text("Hello Cruel World!");
	//ImGui::End();
}