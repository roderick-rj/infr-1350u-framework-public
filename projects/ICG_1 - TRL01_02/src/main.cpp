#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include "Logging.h"

int main()
{
	Logger::Init(); // initializes the logger
	// Initialize GLFW
	if (glfwInit() == GLFW_FALSE) {
		std::cout << "Failed to initialize Glad" << std::endl;
		return 1;
	}
	// Create a new GLFW window
	// GLFWwindow * = (width, height, window_name, fullscreen_bool, share_between_monitors)
	GLFWwindow* window = glfwCreateWindow(300, 300, "ICG_1 - TRL01", nullptr, nullptr);
	// We want GL commands to be executed for our window, so we make our window's context the current one
	glfwMakeContextCurrent(window);
	
	// Let glad know what function loader we are using (will call gl commands via glfw)
	if (gladLoadGLLoader((GLADloadproc)glfwGetProcAddress) == 0) {
		std::cout << "Failed to initialize Glad" << std::endl;
		return 2;
	}
	// Display our GPU and OpenGL version
	std::cout << glGetString(GL_RENDERER) << std::endl;
	std::cout << glGetString(GL_VERSION) << std::endl;

	// requires Logger::Init() at the start of the main() function.
	LOG_TRACE("Hello World! I have {} problems.", 99);
	LOG_INFO("Hello World! I have {} problems.", 99);
	LOG_WARN("Hello World! I have {} problems.", 99);
	LOG_ERROR("Hello World! I have {} problems.", 99);

	// Run as long as the window is open
	while (!glfwWindowShouldClose(window)) {
		// Poll for events from windows (clicks, keypressed, closing, all that)
		glfwPollEvents();
		// Clear our screen every frame
		// glClearColor(0.1f, 0.7f, 0.5f, 1.0f); // ClearColour(R, G, B, A) (greenish ~ original set)
		glClearColor(0.9f, 0.2f, 0.1f, 1.0f); // ClearColour(R, G, B, A) 
		glClear(GL_COLOR_BUFFER_BIT);
		// Present our image to windows
		glfwSwapBuffers(window);
	}

	// std::cin.get(); // used to prevent the OpenGL window from closing immediately

	return 0;
}