/*
 * Name: Roderick "R.J." Montague
 * Student Number: 100701758
 * Date: 10/10/2019
 * Description: the mesh class and vertex struct. These are about the same as what was provided in the tutorial.
*/

// VERTEX STRUCT AND MESH CLASS (SOURCE)
#pragma once
#include <glad/glad.h>
#include <GLM/glm.hpp> // For vec3 and vec4
#include <cstdint> // Needed for uint32_t
#include <memory> // Needed for smart pointers

// Vertex Struct - saves vertex position and colour.
struct Vertex {
	glm::vec3 Position;
	glm::vec4 Color;
};

// Mesh Class - creates meshes so that objects can appear on screen.
class Mesh {
public:
	// Shorthand for shared_ptr
	typedef std::shared_ptr<Mesh> Sptr;
	
	// Creates a new mesh from the given vertices and indices
	Mesh(Vertex* vertices, size_t numVerts, uint32_t* indices, size_t numIndices);

	// destructor
	~Mesh();

	// Draws this mesh
	void Draw();

private:
	// Our GL handle for the Vertex Array Object
	GLuint myVao;

	// 0 is vertices, 1 is indices
	GLuint myBuffers[2];

	// The number of vertices and indices in this mesh
	size_t myVertexCount, myIndexCount;
};