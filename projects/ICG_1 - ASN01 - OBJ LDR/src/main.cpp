/*
 * Name: Roderick "R.J." Montague
 * Student Number: 100701758
 * Date: 10/10/2019
 * Description: object loader main.cpp. This is about the same as what we did in the tutorial.
*/

// INFR 1350U: INTRODUCTION TO COMPUTER GRAPHICS
// ASSIGNMENT 1: OBJECT LOADER
#include "Game.h"
#include "Logging.h"

#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

int main()
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF); // checks for memory leaks once the program ends.
	long long allocPoint = 0;
	if (allocPoint)
	{
		_CrtSetBreakAlloc(allocPoint); // sets where you want to stop the program by assigning the allocation block index stopping point.
	}

	Logger::Init();

	// creates and runs the game.
	Game* game = new Game();
	game->Run();
	delete game; // removes the game from memory.

	Logger::Uninitialize();

	return 0;
}