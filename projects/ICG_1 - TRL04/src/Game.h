#pragma once
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include "GLM/glm.hpp"

#include "Shader.h"
#include "Mesh.h"
#include "Camera.h"

class Game {
public:
	Game();
	
	~Game();
	
	void Run();

	void HandleResize(int width, int height);

protected:
	void Initialize();
	
	void Shutdown();
	
	void LoadContent();
	
	void UnloadContent();
	
	void InitImGui();
	
	void ShutdownImGui();
	
	void ImGuiNewFrame();
	
	void ImGuiEndFrame();
	
	void Update(float deltaTime);
	
	void Draw(float deltaTime);
	
	void DrawGui(float deltaTime);

private:
	// Stores the main window that the game is running in
	GLFWwindow* myWindow;

	// Stores the clear color of the game's window
	glm::vec4 myClearColor;
	
	// Stores the title of the game's window
	char myWindowTitle[32];

	// A shared pointer to our mesh
	Mesh::Sptr myMesh;
	
	// A shared pointer to our shader
	Shader::Sptr myShader;

	// Extra Shader for exercise.
	Mesh::Sptr myMesh2;
	// Shader::Sptr myShader2; // not needed

	Camera::Sptr myCamera;
	bool perspectiveCamera = false; // if 'true', the perpsective camera is used. If 'false', orthographic camera is used.

	// Our models transformation matrix
	glm::mat4 myModelTransform;
};