#include "Game.h"
#include <stdexcept>
#define IMGUI_IMPL_OPENGL_LOADER_GLAD
#include "imgui.h"
#include "imgui_impl_opengl3.h"
#include "imgui_impl_glfw.h"

#include "GLM/gtc/matrix_transform.hpp"
// #include <string>

// call this function to  resize the window.
void GlfwWindowResizedCallback(GLFWwindow* window, int width, int height) {
	glViewport(0, 0, width, height);
	Game* game = (Game*)glfwGetWindowUserPointer(window);

	if (game != nullptr)
	{
		game->HandleResize(width, height);
	}
}

Game::Game() :
	myWindow(nullptr),
	myWindowTitle("ICG_1 - TRL04"),
	myClearColor(glm::vec4(0.1f, 0.7f, 0.5f, 1.0f))
{ }

Game::~Game() { }

void Game::Initialize() {
	// Initialize GLFW
	if (glfwInit() == GLFW_FALSE) {
		std::cout << "Failed to initialize GLFW" << std::endl;
		throw std::runtime_error("Failed to initialize GLFW");
	}
	// Enable transparent backbuffers for our windows (note that Windows expects our colors to be pre-multiplied with alpha)
	glfwWindowHint(GLFW_TRANSPARENT_FRAMEBUFFER, true);
	// Create a new GLFW window
	myWindow = glfwCreateWindow(600, 600, myWindowTitle, nullptr, nullptr);
	// We want GL commands to be executed for our window, so we make our window's context the current one
	glfwMakeContextCurrent(myWindow);
	// Let glad know what function loader we are using (will call gl commands via glfw)
	if (gladLoadGLLoader((GLADloadproc)glfwGetProcAddress) == 0) {
		std::cout << "Failed to initialize Glad" << std::endl;
		throw std::runtime_error("Failed to initialize GLAD");
	}

	// setting up window user pointer so that we can resize our window
	// Tie our game to our window, so we can access it via callbacks
	glfwSetWindowUserPointer(myWindow, this);
	// Set our window resized callback
	glfwSetWindowSizeCallback(myWindow, GlfwWindowResizedCallback);

}

void Game::Shutdown() {
	glfwTerminate();
}

// loads the content for the meshes and shaders
void Game::LoadContent() 
{
	myCamera = std::make_shared<Camera>();
	myCamera->SetPosition(glm::vec3(5, 5, 5));
	myCamera->LookAt(glm::vec3(0));

	// sets the camera to perspective mode.
	myCamera->Projection = glm::perspective(glm::radians(60.0f), 1.0f, 0.01f, 1000.0f);
	perspectiveCamera = true; // in perspective mode.


	// Create our 4 vertices
	// we're using an initalizer list inside an initializer list to get the data
	Vertex vertices[4] = {
		// Position Color
		//  x      y	 z		   r	 g	   b	 a
		{{ -0.5f, -0.5f, 0.0f }, { 1.0f, 0.0f, 0.0f, 1.0f }},
		{{ 0.5f, -0.5f, 0.0f }, { 1.0f, 1.0f, 0.0f, 1.0f }},
		{{ -0.5f, 0.5f, 0.0f }, { 1.0f, 0.0f, 1.0f, 1.0f }},
		{{ 0.5f, 0.5f, 0.0f }, { 0.0f, 1.0f, 0.0f, 1.0f }},
	};
	// Create our 6 indices
	uint32_t indices[6] = {
	0, 1, 2,
	2, 1, 3
	};
	// Create a new mesh from the data
	myMesh = std::make_shared<Mesh>(vertices, 4, indices, 6);


	/// EX ///
	// second set of vertices
	Vertex vertices2[4] = 
	{
		// Position Color
		{{ -0.5f + 0.5f, -0.5f + 0.5f, 0.0f }, { 1.0f / 2.0f, 0.0f, 0.0f, 1.0f }},
		{{ 0.5f + 0.5f, -0.5f + 0.5f, 0.0f }, { 1.0f / 2.0f, 1.0f, 0.0f, 1.0f }},
		{{ -0.5f + 0.5f, 0.5f + 0.5f, 0.0f }, { 1.0f / 2.0f, 0.0f, 1.0f / 1.5f, 1.0f }},
		{{ 0.5f + 0.5f, 0.5f + 0.5f, 0.0f }, { 0.0f, 1.0f / 1.5f, 0.0f, 1.0f }},
	};
	
	// second set of indices
	// Create our 6 indices
	uint32_t indices2[6] = 
	{
	0, 1, 2,
	2, 1, 3
	};
	
	myMesh2 = std::make_shared<Mesh>(vertices2, 4, indices, 6);

	// No longer needed since we have dedicated .glsl files.
	// all the new lines and whatnot will go into one string using R'LIT
	/*const char* vs_source = R"LIT(
		#version 410
		layout (location = 0) in vec3 inPosition;
		layout (location = 1) in vec4 inColor;

		layout (location = 0) out vec4 outColor;
		void main() {
		outColor = inColor;
		gl_Position = vec4(inPosition, 1);
		}
		)LIT";

	// output colour is just what we passed in, we aren't modifying it for now.
	const char* fs_source = R"LIT(
		#version 410
		layout (location = 0) in vec4 inColor;
		layout (location = 0) out vec4 outColor;
		void main() {
		outColor = inColor;
		}
		)LIT";*/

	// Create and compile shader
	myShader = std::make_shared<Shader>();
	// myShader->Compile(vs_source, fs_source); // no longer needed since we have a dedicated file.
	myShader->Load("passthrough.vert.glsl", "passthrough.frag.glsl");
	
	myModelTransform = glm::mat4(1.0f);
}

void Game::UnloadContent() {
}

void Game::Update(float deltaTime) {
	glm::vec3 movement = glm::vec3(0.0f);
	glm::vec3 rotation = glm::vec3(0.0f);
	static glm::vec3 objMovement = glm::vec3(0.0f);

	float speed = 2.5f; // changed from 1.0F
	float rotSpeed = 2.5f;

	// set to 'true' to have the object rotate. This prevents you from moving forward and backward.
	bool objRotate = true;

	if (glfwGetKey(myWindow, GLFW_KEY_W) == GLFW_PRESS)
		movement.z -= speed * deltaTime;
	if (glfwGetKey(myWindow, GLFW_KEY_S) == GLFW_PRESS)
		movement.z += speed * deltaTime;
	if (glfwGetKey(myWindow, GLFW_KEY_A) == GLFW_PRESS)
		movement.x -= speed * deltaTime;
	if (glfwGetKey(myWindow, GLFW_KEY_D) == GLFW_PRESS)
		movement.x += speed * deltaTime;
	if (glfwGetKey(myWindow, GLFW_KEY_SPACE) == GLFW_PRESS)
		movement.y += speed * deltaTime;
	if (glfwGetKey(myWindow, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS)
		movement.y -= speed * deltaTime;

	if (glfwGetKey(myWindow, GLFW_KEY_Q) == GLFW_PRESS)
		rotation.z -= rotSpeed * deltaTime;
	if (glfwGetKey(myWindow, GLFW_KEY_E) == GLFW_PRESS)
		rotation.z += rotSpeed * deltaTime;
	if (glfwGetKey(myWindow, GLFW_KEY_UP) == GLFW_PRESS)
		rotation.x -= rotSpeed * deltaTime;
	if (glfwGetKey(myWindow, GLFW_KEY_DOWN) == GLFW_PRESS)
		rotation.x += rotSpeed * deltaTime;
	if (glfwGetKey(myWindow, GLFW_KEY_LEFT) == GLFW_PRESS)
		rotation.y -= rotSpeed * deltaTime;
	if (glfwGetKey(myWindow, GLFW_KEY_RIGHT) == GLFW_PRESS)
		rotation.y += rotSpeed * deltaTime;

	// if 'c' is pressed, the camera mode is changed.
	
	static bool isKeyDown = false; // makes it so that the key isn't constantly detected if it is released.
	if (glfwGetKey(myWindow, GLFW_KEY_C) == GLFW_PRESS)
	{
		if (!isKeyDown) {
			perspectiveCamera = !perspectiveCamera;

			// perpsective (fovRadians, aspect, zNear, zFar
			// ortho (left, right, bottom, top, near, far)

			// switches camera mode
			myCamera->Projection = (perspectiveCamera) ?
				glm::perspective(glm::radians(60.0f), 1.0f, 0.01f, 1000.0f) : glm::ortho(-5.0f, 5.0f, -5.0f, 5.0f, 0.0f, 100.0f);
		}
		isKeyDown = true;
	}
	else {
		isKeyDown = false;
	}

	if(!objRotate)
		myModelTransform = glm::mat4(1.0f); // resets the transform matrix so that it's an identity matrix.
	
											// translation of the object.
	if (glfwGetKey(myWindow, GLFW_KEY_I) == GLFW_PRESS) // move up (y-axis up)
	{
		if (objRotate)
		{
			myModelTransform = glm::translate(myModelTransform, glm::vec3(0.0f, 0.0f, speed * deltaTime));
		}
		else
		{
			objMovement += glm::vec3(0.0f, 0.0f, speed * deltaTime);
		}	
	}
	else if (glfwGetKey(myWindow, GLFW_KEY_K) == GLFW_PRESS) // move down (y-axis down)
	{
		if (objRotate)
		{
			myModelTransform = glm::translate(myModelTransform, glm::vec3(0.0f, 0.0f, -speed * deltaTime));
		}
		else
		{
			objMovement += glm::vec3(0.0f, 0.0f, -speed * deltaTime);
		}
	}
	else if (glfwGetKey(myWindow, GLFW_KEY_J) == GLFW_PRESS) // move left (x-axis left)
	{
		if (objRotate)
		{
			myModelTransform = glm::translate(myModelTransform, glm::vec3(-speed * deltaTime, 0.0f, 0.0f));
		}
		else
		{
			objMovement += glm::vec3(-speed * deltaTime, 0.0f, 0.0f);
		}
	}
	else if (glfwGetKey(myWindow, GLFW_KEY_L) == GLFW_PRESS) // move right (x-axis right)
	{
		if (objRotate)
		{
			myModelTransform = glm::translate(myModelTransform, glm::vec3(speed * deltaTime, 0.0f, 0.0f));
		}
		else
		{
			objMovement += glm::vec3(speed * deltaTime, 0.0f, 0.0f);
		}
	}

	if(!objRotate)
		myModelTransform = glm::translate(myModelTransform, objMovement);
	
	// Rotate and move our camera based on input
	myCamera->Rotate(rotation);
	myCamera->Move(movement);

	// Rotate our transformation matrix a little bit each frame
	// Doesn't work if myModelTransform is set to an identity matrix
	myModelTransform = glm::rotate(myModelTransform, deltaTime, glm::vec3(0, 0, 1));
}

void Game::InitImGui() {
	// Creates a new ImGUI context
	ImGui::CreateContext();
	// Gets our ImGUI input/output
	ImGuiIO& io = ImGui::GetIO();
	// Enable keyboard navigation
	io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
	// Allow docking to our window
	io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;
	// Allow multiple viewports (so we can drag ImGui off our window)
	io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;
	// Allow our viewports to use transparent backbuffers
	io.ConfigFlags |= ImGuiConfigFlags_TransparentBackbuffers;
	// Set up the ImGui implementation for OpenGL
	ImGui_ImplGlfw_InitForOpenGL(myWindow, true);
	ImGui_ImplOpenGL3_Init("#version 410");

	// Dark mode FTW
	ImGui::StyleColorsDark();
	// Get our imgui style
	ImGuiStyle& style = ImGui::GetStyle();
	//style.Alpha = 1.0f;
	if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable) {
		style.WindowRounding = 0.0f;
		style.Colors[ImGuiCol_WindowBg].w = 0.8f;
	}
}

void Game::ShutdownImGui() {
	// Cleanup the ImGui implementation
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	// Destroy our ImGui context
	ImGui::DestroyContext();
}

void Game::ImGuiNewFrame() {
	// Implementation new frame
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplGlfw_NewFrame();
	// ImGui context new frame
	ImGui::NewFrame();
}

void Game::ImGuiEndFrame() {
	// Make sure ImGui knows how big our window is
	ImGuiIO& io = ImGui::GetIO();
	int width{ 0 }, height{ 0 };
	glfwGetWindowSize(myWindow, &width, &height);
	io.DisplaySize = ImVec2(width, height);
	// Render all of our ImGui elements
	ImGui::Render();
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
	// If we have multiple viewports enabled (can drag into a new window)
	if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable) {
		// Update the windows that ImGui is using
		ImGui::UpdatePlatformWindows();
		ImGui::RenderPlatformWindowsDefault();
		// Restore our gl context
		glfwMakeContextCurrent(myWindow);
	}
}

void Game::Run()
{
	Initialize();
	InitImGui();
	LoadContent();
	static float prevFrame = glfwGetTime();
	// Run as long as the window is open
	while (!glfwWindowShouldClose(myWindow)) {
		// Poll for events from windows
		// clicks, key presses, closing, all that
		glfwPollEvents();
		float thisFrame = glfwGetTime();
		float deltaTime = thisFrame - prevFrame;
		Update(deltaTime);
		Draw(deltaTime);
		ImGuiNewFrame();
		DrawGui(deltaTime);
		ImGuiEndFrame();
		prevFrame = thisFrame;
		// Present our image to windows
		glfwSwapBuffers(myWindow);

		// added so that the previous frame is updated. Otherwise, it would just be since the beginning of the program.
		prevFrame = thisFrame;
	}
	UnloadContent();
	ShutdownImGui();
	Shutdown();
}

// resizes the window and keeps size proportionate.
void Game::HandleResize(int width, int height)
{
	// set to float since we're calculating the new projecction as the screen size.
	if(perspectiveCamera) // camera is in perspective mode
	{
		myCamera->Projection = glm::perspective(glm::radians(60.0f), width / (float)height, 0.01f, 1000.0f);
	}
	else // camera is in orthographic mode
	{
		myCamera->Projection = glm::ortho(-5.0f * width / (float)height, 5.0f * width / (float)height, -5.0f, 5.0f, 0.0f, 100.0f);
	}
	
}

void Game::Draw(float deltaTime) {
	// Clear our screen every frame
	glClearColor(myClearColor.x, myClearColor.y, myClearColor.z, myClearColor.w);
	glClear(GL_COLOR_BUFFER_BIT);

	myShader->Bind();

	// myShader->SetUniform("a_ModelViewProjection", myCamera->GetViewProjection());
	myShader->SetUniform("a_ModelViewProjection", myCamera->GetViewProjection() * myModelTransform); // allows for slow rotation

	myMesh->Draw();
	// Update our uniform

	// myMesh2->Draw();
}

void Game::DrawGui(float deltaTime) {
	// Open a new ImGui window
	ImGui::Begin("Colour Picker");
	
	// Draw widgets here
	// ImGui::SliderFloat4("Color", &myClearColor.x, 0, 1); // Original
	ImGui::ColorPicker4("Color", &myClearColor.x); // new version
	if (ImGui::InputText("Title", myWindowTitle, 31))
	{
		glfwSetWindowTitle(myWindow, myWindowTitle);
	}

	if (ImGui::Button("Apply")) // adding another button, which allows for the application of the window title.
	{
		glfwSetWindowTitle(myWindow, myWindowTitle);
	}

	ImGui::Text("C: Change Camera Mode");
	// ImGui::Text(("Time: " + std::to_string(glfwGetTime())).c_str()); // requires inclusion of <string>

	ImGui::End();

	// Creating a Second Window
	//ImGui::Begin("Test 2");
	//ImGui::Text("Hello Cruel World!");
	//ImGui::End();
}