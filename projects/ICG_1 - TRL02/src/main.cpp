#include "Game.h"
#include "Logging.h"

#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

int main()
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF); // checks for memory leaks once the program ends.
	long long allocPoint = 0;
	if (allocPoint)
	{
		_CrtSetBreakAlloc(allocPoint); // sets where you want to stop our program by assigning the allocation block index stopping point.
	}

	// double * foobars = new double[1024]; // this leaks off memory so that we can test for memory leaks. Don't use this.

	Logger::Init();

	Game* game = new Game();
	game->Run();
	delete game;

	Logger::Uninitialize();

	return 0;
}