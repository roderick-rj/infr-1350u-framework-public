#version 410
in vec3 color;
in vec2 texUV;
in vec3 world_pos;
in vec3 world_normal;

uniform vec3 light_pos;
uniform vec3 eye_pos;

out vec4 frag_color;

uniform sampler2D myTextureSampler;

const float lightIntensity = 10.0;

// Toon
const int levels = 5; // the amount of bends
const float scaleFactor = 1.0/levels;



void main() {
	
	vec3 L = normalize(light_pos - world_pos); // vertex position
	vec3 V = normalize(eye_pos - world_pos); // camera position (view direction)

	float dist = length(light_pos - world_pos);

	float diffuse = max(0, dot(L, world_normal));
	vec3 diffuseOut = (diffuse * color) / (dist*dist);
	

	diffuseOut = diffuseOut*lightIntensity;
	
	// Cel/Toon Shading
	diffuseOut = floor(diffuseOut * levels) * scaleFactor;

	// Silouette
	// the normal was already transformed (i.e. the model was multiplied by it) in the vertex shader.
	float edge = dot(V, world_normal);

	// if the dot product is between 0 and 0.2, then a value of 0 is assigned.
	
	// if(edge > 0.2) // no outline
	if(edge > 0.01 && edge < 0.3) // slight outline
		edge = 0.0;
	else
		edge = 1.0;

	// shading (deiffuse 3 is a colour)
	// vec3 result = diffuseOut; // cel-shading
	vec3 result = diffuseOut * edge; // outline

	// no texture
	// frag_color = texture(myTextureSampler, texUV); // texture, but no cel-shading

	// frag_color = vec4(result, 1.0); // no texture, but cel-shading and silouettes

	frag_color = texture(myTextureSampler, texUV) * vec4(result, 1.0);

	
}