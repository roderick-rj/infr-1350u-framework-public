#version 450
in vec3 color;

out vec4 frag_color;

uniform sampler2D myTextureSampler;

void main() {
	frag_color = vec4(color, 1.0);

}